#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 14 11:30:49 2017

@author: i42mesup
"""

import numpy as np
import pandas as pd
import time

from sklearn import svm
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import StratifiedKFold

# Parametros utilizados (modificar)
seed = 30

# Cargar el dataset de entrenamiento
train_data = pd.read_csv('BasesDatos/csv/train_nomnist.csv',header=None)
X_train = train_data.values[:, :-1]
y_train = train_data.values[:, -1]

# Cargar el dataset de testing
test_data = pd.read_csv('BasesDatos/csv/test_nomnist.csv', header=None)
X_test = test_data.values[:, :-1]
y_test = test_data.values[:, -1]

# Estandarizar los atributos
scaler = StandardScaler()
X_train = scaler.fit_transform(X_train)
X_test = scaler.transform(X_test)

# Probar con varios tamaños de particion
for num_folds in [3, 5, 10]:
    # Inicio medida de tiempo
    time_start = time.perf_counter()
    
    # Aplicacion de K-fold sobre train para obtener K conjuntos para validacion cruzada
    # Fijamos la semilla a 10 para evaluacion de resultados consistente
    skf = StratifiedKFold(n_splits=num_folds, random_state=seed, shuffle=True)
    # Almacenamiento de las K particiones, para poder iterar sobre ellas posteriormente
    fold_list = list(skf.split(X_train, y_train))
    
    # Generar rejilla de valores de C y gamma a evaluar
    Cs = np.logspace(-5, 15, num=11, base=2)
    Gs = np.logspace(-15, 3, num=9, base=2)
    cc, gg = np.meshgrid(Cs, Gs, indexing='ij')
    
    # Mejores valores de CCR, C y Gamma encontrados
    best_avg_ccr = -1
    best_c = None
    best_gamma = None
    # Iterar sobre cada combinacion de C y gamma, para encontrar la mejor combinacion
    # de estos en train
    print('Buscando valores optimos de C y gamma mediante %d-fold...' % (skf.n_splits))
    for c in range(cc.shape[0]):
        for g in range(gg.shape[1]):
            # Error medio para los K entrenamientos
            avg_ccr = 0
            
            # Realizar K entrenamientos/pruebas
            for k in range(skf.n_splits):
                # Determinar que patrones forman train y test
                index_train_kfold, index_test_kfold = fold_list[k]
                
                # Emplear el conjunto k como test y el resto como training
                X_train_kfold = X_train[index_train_kfold]
                y_train_kfold = y_train[index_train_kfold]
                X_test_kfold = X_train[index_test_kfold]
                y_test_kfold = y_train[index_test_kfold]
                
                # Entrenar el modelo SVM (C-SVC)
                svm_model = svm.SVC(kernel='rbf',C=cc[c,g],gamma=gg[c,g])
                svm_model.fit(X_train_kfold, y_train_kfold)
                
                # Acumular CCR de test
                avg_ccr += svm_model.score(X_test_kfold, y_test_kfold)
                
            # Calcular CCR medio
            avg_ccr /= skf.n_splits
            
            # Comprobar si mejora el mejor valor conocido
            if avg_ccr > best_avg_ccr:
                best_c = cc[c,g]
                best_gamma = gg[c,g]
                best_avg_ccr = avg_ccr
    
    
    # Imprimir valores determinados como optimos
    print('Valores optimos encontrados. C = %f, gamma = %f. CCR = %.3f%%' % (best_c, 
          best_gamma, best_avg_ccr * 100))
    
    # Entrenamiento empleando el conjunto de train completo y los parametros optimos
    svm_model = svm.SVC(kernel='rbf',C=best_c,gamma=best_gamma)
    svm_model.fit(X_train, y_train)
    
    # Evaluacion sobre conjunto de test
    ccr_train = svm_model.score(X_train, y_train) * 100
    ccr_test = svm_model.score(X_test, y_test) * 100
    
    # Fin medida de tiempo
    total_time = time.perf_counter()- time_start
    
    # Mostrar resultados
    print('- Fase de test real (%d-fold). CCR Train = %.3f %%, CCR Test = %.3f %%. Tiempo = %.6f segundos.' 
          % (num_folds, ccr_train, ccr_test, total_time))
