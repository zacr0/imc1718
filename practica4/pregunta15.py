#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec 13 19:07:31 2017

@author: i42mesup
"""

import numpy as np
import pandas as pd
import time

from sklearn import svm
from sklearn.preprocessing import StandardScaler

# Cargar el dataset de entrenamiento
train_data = pd.read_csv('BasesDatos/csv/train_spam.csv',header=None)
X_train = train_data.values[:, :-1]
y_train = train_data.values[:, -1]

# Cargar el dataset de testing
test_data = pd.read_csv('BasesDatos/csv/test_spam.csv', header=None)
X_test = test_data.values[:, :-1]
y_test = test_data.values[:, -1]

# Estandarizar los atributos
scaler = StandardScaler()
X_train = scaler.fit_transform(X_train)
X_test = scaler.transform(X_test)

# Probar con los valores de C^-2 a C^1
for i in np.arange(-2,2,1, 'double'):
    # Inicio medida de tiempo
    time_start = time.perf_counter()
    
    # Entrenar el modelo SVM (C-SVC)
    # Se emplean los valores interpretados como optimos para este dataset
    svm_model = svm.SVC(kernel='linear',C=10**i)
    svm_model.fit(X_train, y_train)
    
    # Calcular el CCR de train y test
    ccr_train = svm_model.score(X_train, y_train) * 100
    ccr_test = svm_model.score(X_test, y_test) * 100
    
    # Fin medida de tiempo
    total_time = time.perf_counter() - time_start
    
    # Mostrar resultados
    print('- C = %.2f. CCR Train = %.3f %%, CCR Test = %.3f %%. Tiempo = %.6f segundos.' 
          % (10**i, ccr_train, ccr_test, total_time))

