#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec 11 16:45:38 2017

@author: i42mesup
"""

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

from sklearn import svm

# Cargar el dataset
data = pd.read_csv('BasesDatos/csv/dataset3.csv',header=None)
X = data.iloc[:,:-1].values
y = data.iloc[:,-1].values

# Probar valores de C desde 2*10^-2 hasta 2*10^2
for i in np.arange(-2, 3, 1, float):
    # Probar valores de C y gamma desde 2*10^-2 hasta 2*10^2
    for j in np.arange(-2, 3, 1, float):
        # Entrenar el modelo SVM (C-SVC)
        svm_model = svm.SVC(kernel='rbf',C=2*10**i,gamma=2*10**j)
        svm_model.fit(X, y)
        
        # Representar los puntos
        plt.figure(1)
        plt.clf()
        plt.scatter(X[:, 0], X[:, 1], c=y, zorder=10, cmap=plt.cm.Paired)
        
        # Representar el hiperplano separador
        plt.axis('tight')
        # Extraer límites
        x_min = X[:, 0].min()
        x_max = X[:, 0].max()
        y_min = X[:, 1].min()
        y_max = X[:, 1].max()
        
        # Crear un grid con todos los puntos y obtener el valor Z devuelto por la SVM
        XX, YY = np.mgrid[x_min:x_max:500j, y_min:y_max:500j]
        Z = svm_model.decision_function(np.c_[XX.ravel(), YY.ravel()])
        
        # Hacer un plot a color con los resultados
        Z = Z.reshape(XX.shape)
        plt.pcolormesh(XX, YY, Z > 0)
        plt.contour(XX, YY, Z, colors=['k', 'r', 'k'], linestyles=['--', '-', '--'],
                        levels=[-.5, 0, .5])
        
        print('- Resultados obtenidos para C = %d, gamma = %f:' % (2*10**i, 2*10**j))
        plt.show()
