#!/bin/bash

#######SCRIPT creado por David Serrano Gemes ##########
#GIT: 	https://github.com/davidserranogemes
#		https://gitlab.com/hidesagred97

#NUM_IT es el numero de iteraciones que hara la red neuronal
#FILE_ROUTE es donde estan las bases de datos
#ARQUITECTURA es la ruta de la arquitectura a usa por defecto, incluyendo los parametros
#EXE: ruta del ejecutable
#DIR es la ruta donde se guardaran los resultados. TODOS se vuelcan en ficheros, cada ejecucion en el suyo

#El programa funciona sin argumentos usando todos los train de FILE_ROUTE y los parametros de arquitectura, haciendo todas las posibles combinaciones entre ellos

#El primer parametro opcional del programa es el nombre de un fichero especifico en FILE_ROUTE, por ejemplo forest. Eso hace que solo se ejecute con la base de datos FOREST
#El segundo parametro opcional del programa es el nombre del fichero de arquitectura. La ruta comienza en la carpeta donde este el script

#Se genera una carpeta resultados, con la hora de inicio de ejecucion  y una estructura de carpetas segun la base de datos. Se crea un fichero de log resumen, que indica el tiempo que se ha tardado y cuantas ejecuciones se han hecho

#Fichero de arquitecturas 
#C: booleano, marca si hay clasificacion (a 1) o no (a 0)
#R: marca el ratio rbf valores entre 0 y 1
#L: booleano, indica si se usa l2 (a 1) o no (a 0)
#E: valor de eta entre 1 y 10^-10
#O: valor de output, siempre a 1, salvo en la arquitectura de parkinsons que se debe poner a 2
#Se hacen todas las combinaciones posibles entre los parametros



NUM_IT='1000'
FILE_ROUTE='./BasesDatos/csv/'
ARQUITECTURA='arquitecturas.txt'
EXE='./rbf.py'
#EXE='./test_parametros.py'

DATE=`date '+%Y-%m-%d_%H:%M:%S'`
DIR='./resultados/resultados_'$DATE

mkdir $DIR



FILES=$(ls $FILE_ROUTE | grep "test" | sed -r 's/^(test_)(.*)(\.csv)/\2/')

if [ $# = 1 ]
	then
	FILES=$1
else if [ $# = 2 ]
	 then
	 	FILES=$1
	 	ARQUITECTURA=$2
	 fi
fi
echo "Inicio de las ejecuciones: `date '+%Y-%m-%d_%H:%M:%S'`" > $DIR/log.txt
for f in $FILES
do
	mkdir $DIR/$f
	echo "	Inicio de las ejecuciones de $f: `date '+%Y-%m-%d_%H:%M:%S'`" >> $DIR/log.txt

	for r in $( cat $ARQUITECTURA | grep '^R:' | sed -r 's/^(R\:)(.*)/\2/')
	do

		for o in $( cat $ARQUITECTURA | grep '^O:' | sed -r 's/^(O\:)(.*)/\2/')
		do
			for e in $( cat $ARQUITECTURA | grep '^E:' | sed -r 's/^(E\:)(.*)/\2/')
			do
				CLASIFICACION=$(cat $ARQUITECTURA | grep '^C:' | sed -r 's/^(C\:)(.*)/\2/')
				REGULARIZACION=$(cat $ARQUITECTURA | grep '^L:' | sed -r 's/^(L\:)(.*)/\2/')
				if [ $CLASIFICACION = 1 ]; then
					if [[ $REGULARIZACION = 1 ]]; then
						$EXE -t ./BasesDatos/csv/train_$f.csv -T ./BasesDatos/csv/test_$f.csv -r $r -o $o -e $e -c --l2 > $DIR/$f/res_$f\_r$r\_o$o\_e$e\_classificacion\_l2.txt
						echo "		r:$r o:$o e:$e clasificacion l2 : `date '+%Y-%m-%d_%H:%M:%S'`" >> $DIR/log.txt
						echo "	 $f	r:$r o:$o e:$e clasificacion l2 : `date '+%Y-%m-%d_%H:%M:%S'`" 
					else
						$EXE -t ./BasesDatos/csv/train_$f.csv -T ./BasesDatos/csv/test_$f.csv -r $r -o $o -e $e -c > $DIR/$f/res_$f\_r$r\_o$o\_e$e\_classificacion\_l1.txt
						echo "		r:$r o:$o e:$e clasificacion  l1: `date '+%Y-%m-%d_%H:%M:%S'`" >> $DIR/log.txt
						echo "	 $f	r:$r o:$o e:$e clasificacion  l1: `date '+%Y-%m-%d_%H:%M:%S'`"  											
					fi

				else
					if [[ $REGULARIZACION = 1 ]]; then
						$EXE -t ./BasesDatos/csv/train_$f.csv -T ./BasesDatos/csv/test_$f.csv -r $r -o $o -e $e --l2 > $DIR/$f/res_$f\_r$r\_o$o\_e$e\_l2.txt
						echo "		r:$r o:$o e:$e l2 : `date '+%Y-%m-%d_%H:%M:%S'`" >> $DIR/log.txt
						echo "	 $f	r:$r o:$o e:$e l2 : `date '+%Y-%m-%d_%H:%M:%S'`" 
					else
						$EXE -t ./BasesDatos/csv/train_$f.csv -T ./BasesDatos/csv/test_$f.csv -r $r -o $o -e $e > $DIR/$f/res_$f\_r$r\_o$o\_e$e\_l1.txt
						echo "		r:$r o:$o e:$e l1: `date '+%Y-%m-%d_%H:%M:%S'`" >> $DIR/log.txt
						echo "	 $f	r:$r o:$o e:$e l1: `date '+%Y-%m-%d_%H:%M:%S'`"   											
					fi
				fi
			done
		done
	done
	echo "	Fin de las ejecuciones de $f: `date '+%Y-%m-%d_%H:%M:%S'`" >> $DIR/log.txt
done
echo "Fin de las ejecuciones `date '+%Y-%m-%d_%H:%M:%S'`" >> $DIR/log.txt
