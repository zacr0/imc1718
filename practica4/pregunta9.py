#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 13 16:05:28 2017

@author: i42mesup
"""

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

from sklearn import svm
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import StratifiedShuffleSplit

# Cargar el dataset
data = pd.read_csv('BasesDatos/csv/dataset3.csv',header=None)
X = data.iloc[:,:-1].values
y = data.iloc[:,-1].values

# Crear 5 particiones estratificadas (80% train / 20% test), para poder observar
# el rendimiento obtenido según la partición obtenida
sss = StratifiedShuffleSplit(n_splits=10, test_size=0.2, train_size=None)

# Repetir el proceso de entrenamiento y test por cada particion generada
i = 1
for train_index, test_index in sss.split(X, y):
    # Formar train y test con los indices extraidos
    X_train = X[train_index]
    y_train = y[train_index]
    X_test = X[test_index]
    y_test = y[test_index]
    
    # Estandarizar los atributos
    scaler = StandardScaler()
    X_train = scaler.fit_transform(X_train)
    X_test = scaler.transform(X_test)
    
    # Entrenar el modelo SVM (C-SVC)
    # Se emplean los valores interpretados como optimos para este dataset
    svm_model = svm.SVC(kernel='rbf',C=20,gamma=20)
    svm_model.fit(X_train, y_train)
    
    # Calcular el CCR de train y test
    ccr_train = svm_model.score(X_train, y_train) * 100
    ccr_test = svm_model.score(X_test, y_test) * 100
    
    # Mostrar resultados
    print('- Particion %d, CCR Train = %.3f %%, CCR Test = %.3f %%.' % (i, ccr_train, ccr_test))
    i += 1
    
    # Representar los puntos
    plt.figure(1)
    plt.clf()
    plt.scatter(X_test[:, 0], X_test[:, 1], c=y_test, zorder=10, cmap=plt.cm.Paired)
    
    # Representar el hiperplano separador
    plt.axis('tight')
    
    # Extraer límites
    x_min = X_train[:, 0].min()
    x_max = X_train[:, 0].max()
    y_min = X_train[:, 1].min()
    y_max = X_train[:, 1].max()
    
    # Crear un grid con todos los puntos y obtener el valor Z devuelto por la SVM
    XX, YY = np.mgrid[x_min:x_max:500j, y_min:y_max:500j]
    Z = svm_model.decision_function(np.c_[XX.ravel(), YY.ravel()])
    
    # Hacer un plot a color con los resultados
    Z = Z.reshape(XX.shape)
    plt.pcolormesh(XX, YY, Z > 0)
    plt.contour(XX, YY, Z, colors=['k', 'r', 'k'], linestyles=['--', '-', '--'],
                    levels=[-.5, 0, .5])
    
    # Mostrar el grafico
    plt.show()
