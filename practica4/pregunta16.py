#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec 13 21:33:49 2017

@author: i42mesup
"""

import numpy as np
import pandas as pd
import time

from sklearn import svm
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import confusion_matrix

# Cargar el dataset de entrenamiento
train_data = pd.read_csv('BasesDatos/csv/train_spam.csv',header=None)
X_train = train_data.values[:, :-1]
y_train = train_data.values[:, -1]

# Cargar el dataset de testing
test_data = pd.read_csv('BasesDatos/csv/test_spam.csv', header=None)
X_test = test_data.values[:, :-1]
y_test = test_data.values[:, -1]

# Guardar una copia de los patrones de test antes de estandarizarlos, para
# comprobar posteriormente los valores de cada atributo
X_test_copy = X_test

# Estandarizar los atributos
scaler = StandardScaler()
X_train = scaler.fit_transform(X_train)
X_test = scaler.transform(X_test)

# Inicio medida de tiempo
time_start = time.perf_counter()

# Entrenar el modelo SVM (C-SVC)
# Se emplean los valores interpretados como optimos para este dataset
svm_model = svm.SVC(kernel='linear',C=0.01)
svm_model.fit(X_train, y_train)

# Calcular el CCR de train y test
ccr_train = svm_model.score(X_train, y_train) * 100
ccr_test = svm_model.score(X_test, y_test) * 100

# Fin medida de tiempo (no consideramos el resto de instrucciones puesto que son
# para el analisis de resultados)
total_time = time.perf_counter() - time_start

# Extraccion de salidas predichas
y_predicted = svm_model.predict(X_test)

# Matriz de confusion
conf_mat = confusion_matrix(y_test, y_predicted)

# Obtencion de indices de patrones mal clasificados
errors = np.where(y_test != y_predicted)[0]

# Obtencion de indices de palabras presentes en cada email mal clasificado
words_in_errors = []
for email in errors:
    email_words = np.where(X_test_copy[email] == 1)[0]
    words_in_errors.append(email_words)

# Crear un diccionario donde la clave es el indice del email mal clasificado y
# el valor las palabras que contiene
dict_email_words = dict(zip(errors,words_in_errors))

# Mostrar resultados
print('- CCR Train = %.3f %%, CCR Test = %.3f %%. Tiempo = %.6f segundos.' 
      % (ccr_train, ccr_test, total_time))
print('\n')
print('- Matriz de confusion:')
print(conf_mat)
print('\n')
print('- Indice de patrones mal clasificados:')
print(errors)
print('\n')

# Mostrar palabras presentes en los emails mal clasificados
print('- Palabras presentes en los patrones mal clasificados:')
# Leer el fichero de texto con las referencias de cada palabra del dataset
word_reference = pd.read_csv('etiquetasYVocabulario/vocab.txt', 
                             delim_whitespace=True, header=None).values[:,1]
# Recorrer el diccionario mostrando email y las palabras correspondientes a cada
# indice                             
for email, words in dict_email_words.items():
    print('- Email %d:' % email)
    print(word_reference[words])
    print('\n')
    