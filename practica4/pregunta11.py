#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 13 19:13:58 2017

@author: i42mesup
"""

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

from sklearn import svm
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import StratifiedKFold
from sklearn.model_selection import StratifiedShuffleSplit

# Parametros utilizados (modificar)
seed = 30
num_folds = 5

# Cargar el dataset
data = pd.read_csv('BasesDatos/csv/dataset3.csv',header=None)
X = data.iloc[:,:-1].values
y = data.iloc[:,-1].values

# Crear 1 particion estratificada (80% train / 20% test), con la semilla fijada
# para evaluacion de resultados consistente
sss = StratifiedShuffleSplit(n_splits=1, test_size=0.2, train_size=None, 
                             random_state=seed)
split_list = list(sss.split(X, y))
train_index, test_index = split_list[0]

# Separar X en train y test mediante los indices extraidos
X_train = X[train_index]
y_train = y[train_index]
X_test = X[test_index]
y_test = y[test_index]

# Estandarizar los atributos
scaler = StandardScaler()
X_train = scaler.fit_transform(X_train)
X_test = scaler.transform(X_test)

# Aplicacion de K-fold sobre train para obtener K conjuntos para validacion cruzada
# Fijamos la semilla a 10 para evaluacion de resultados consistente
skf = StratifiedKFold(n_splits=num_folds, random_state=seed, shuffle=True)
# Almacenamiento de las K particiones, para poder iterar sobre ellas posteriormente
fold_list = list(skf.split(X_train, y_train))

# Generar rejilla de valores de C y gamma a evaluar
Cs = np.logspace(-5, 15, num=15, base=2)
Gs = np.logspace(-15, 3, num=15, base=2)
cc, gg = np.meshgrid(Cs, Gs, indexing='ij')

# Mejores valores de CCR, C y Gamma encontrados
best_avg_ccr = -1
best_c = None
best_gamma = None
# Iterar sobre cada combinacion de C y gamma, para encontrar la mejor combinacion
# de estos en train
print('Buscando valores optimos de C y gamma mediante %d-fold...' % (skf.n_splits))
for c in range(cc.shape[0]):
    for g in range(gg.shape[1]):
        # Error medio para los K entrenamientos
        avg_ccr = 0
        
        # Realizar K entrenamientos/pruebas
        for k in range(skf.n_splits):
            # Determinar que patrones forman train y test
            index_train_kfold, index_test_kfold = fold_list[k]
            
            # Emplear el conjunto k como test y el resto como training
            X_train_kfold = X_train[index_train_kfold]
            y_train_kfold = y_train[index_train_kfold]
            X_test_kfold = X_train[index_test_kfold]
            y_test_kfold = y_train[index_test_kfold]
            
            # Entrenar el modelo SVM (C-SVC)
            svm_model = svm.SVC(kernel='rbf',C=cc[c,g],gamma=gg[c,g])
            svm_model.fit(X_train_kfold, y_train_kfold)
            
            # Acumular CCR de test
            avg_ccr += svm_model.score(X_test_kfold, y_test_kfold)
            
        # Calcular CCR medio
        avg_ccr /= skf.n_splits
        
        # Comprobar si mejora el mejor valor conocido
        if avg_ccr > best_avg_ccr:
            best_c = cc[c,g]
            best_gamma = gg[c,g]
            best_avg_ccr = avg_ccr


# Imprimir valores determinados como optimos
print('Valores optimos encontrados. C = %f, gamma = %f. CCR = %.3f%%' % (best_c, 
      best_gamma, best_avg_ccr * 100))

# Entrenamiento empleando el conjunto de train completo y los parametros optimos
svm_model = svm.SVC(kernel='rbf',C=best_c,gamma=best_gamma)
svm_model.fit(X_train, y_train)

# Evaluacion sobre conjunto de test
ccr_train = svm_model.score(X_train, y_train) * 100
ccr_test = svm_model.score(X_test, y_test) * 100

# Mostrar resultados
print('- Fase de test real. CCR Train = %.3f %%, CCR Test = %.3f %%.' % (ccr_train, ccr_test))

# Representar los puntos
plt.figure(1)
plt.clf()
plt.scatter(X_test[:, 0], X_test[:, 1], c=y_test, zorder=10, cmap=plt.cm.Paired)

# Representar el hiperplano separador
plt.axis('tight')

# Extraer límites
x_min = X_train[:, 0].min()
x_max = X_train[:, 0].max()
y_min = X_train[:, 1].min()
y_max = X_train[:, 1].max()

# Crear un grid con todos los puntos y obtener el valor Z devuelto por la SVM
XX, YY = np.mgrid[x_min:x_max:500j, y_min:y_max:500j]
Z = svm_model.decision_function(np.c_[XX.ravel(), YY.ravel()])

# Hacer un plot a color con los resultados
Z = Z.reshape(XX.shape)
plt.pcolormesh(XX, YY, Z > 0)
plt.contour(XX, YY, Z, colors=['k', 'r', 'k'], linestyles=['--', '-', '--'],
                levels=[-.5, 0, .5])

# Mostrar el grafico
plt.show()
