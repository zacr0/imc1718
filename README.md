imc1718
=============

Repositorio de la asignatura Introducción a los Modelos Computacionales, de cuarto curso del Grado de Ingeniería Informática, mención en Computación, de la Universidad de Córdoba. Sólo se versionan las tareas relacionadas con la programación.
