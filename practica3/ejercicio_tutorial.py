#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 20 16:43:42 2017

@author: i42mesup
"""

# Librerias
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

from sklearn import neighbors      # KNN
from sklearn import preprocessing  # Preprocesamiento
from sklearn import linear_model   # Regresion Logistica
from sklearn.metrics import confusion_matrix # Matriz de confusion


# Dar como cabecera el indice de la columna, y la clase como ultima columna
nombre_variables = list(range(64));
nombre_variables.append('clase');

# Cargar conjunto de datos digits desde fichero local
train_digits = pd.read_csv('basesDatosPr3IMC/csv/train_digits.csv', names=nombre_variables);
test_digits = pd.read_csv('basesDatosPr3IMC/csv/test_digits.csv', names=nombre_variables);

# Valores de entrada de los conjuntos
train_inputs_digits = train_digits.values[:, 0:-1]
test_inputs_digits = test_digits.values[:, 0:-1]

# Normalizacion de variables de entrada
train_inputs_digits = preprocessing.minmax_scale(train_inputs_digits, (0,1), copy=False)
test_inputs_digits = preprocessing.minmax_scale(test_inputs_digits, (0,1), copy=False)

# Valores de salida de los conjuntos
train_outputs_digits = train_digits.values[:, -1]
test_outputs_digits = test_digits.values[:,-1]


# Entrenamiento y evaluacion del algoritmo KNN con numero de vecinos variable
best_ccr_test = 0
best_num_vecinos = 0
best_test_outputs = []
for i in range(1, 15):
    knn = neighbors.KNeighborsClassifier(n_neighbors=i)
    knn.fit(train_inputs_digits, train_outputs_digits)
    ccr_train = knn.score(train_inputs_digits, train_outputs_digits)
    ccr_test = knn.score(test_inputs_digits, test_outputs_digits)
    print("- %d vecinos \t-> \tCCR train: %.2f%% \tCCR test: %.2f%%" % (i, ccr_train*100, ccr_test*100))
    
    # Almacenar la mejor configuracion
    if ccr_test > best_ccr_test:
        best_ccr_test = ccr_test
        best_num_vecinos = i
        best_test_outputs = knn.predict(test_inputs_digits)

# Impresion de matriz de confusion de mejor configuracion
print("Matriz de confusion para la mejor configuracion obtenida (%d vecinos)." % (best_num_vecinos))
print(confusion_matrix(test_outputs_digits, best_test_outputs))


# Entrenamiento de modelo de regresion logistica variando tipo de regularizacion
# y valor de importancia de esta
best_ccr_test = 0;
best_test_outputs = [];
best_pen = '';
best_c = 0;
for pen in ['l1', 'l2']:
    for c in [0.01, 0.1, 1, 10, 100, 1000]:
        log_reg = linear_model.LogisticRegression(penalty=pen, C=c)
        log_reg = log_reg.fit(train_inputs_digits, train_outputs_digits)
        ccr_train = log_reg.score(train_inputs_digits, train_outputs_digits)
        ccr_test = log_reg.score(test_inputs_digits, test_outputs_digits)
        
        # Impresion de resultados
        print("- Regularizacion %s con C=%.2f -> \tCCR train: %.2f%% \tCCR test: %.2f%%" % (pen, c, ccr_train*100, ccr_test*100))
        
        # Almacenar la mejor configuracion
        if ccr_test > best_ccr_test:
            best_ccr_test = ccr_test
            best_test_outputs = log_reg.predict(test_inputs_digits)
            best_pen = pen
            best_c = c
        
# Impresion de matriz de confusion de mejor configuracion
print("Matriz de confusion para la mejor configuracion obtenida (Penalizacion %s, C=%.2f)." % (best_pen, best_c))
print(confusion_matrix(test_outputs_digits, best_test_outputs))