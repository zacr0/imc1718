# -*- coding: utf-8 -*-
# Se corresponde con la resolución del último ejercicio de las diapositivas
# (Ejemplo 2)
import numpy as np
import matplotlib.pyplot as plt

def regresionLineal(X,Y):
    XtX = np.dot(np.transpose(X),X)
    XtY = np.dot(np.transpose(X),Y)
    w = np.dot(np.linalg.pinv(XtX),XtY)
    return w

def gradoX(X,n):    
    X = np.hstack((np.ones((X.shape[0],1)), X))
    for i in range(2,n+1):
        Xi = (X[:,1]**i)
        X = np.hstack((X, Xi.reshape(X.shape[0],1)))
    return X

X = np.array([[0.86,0.09,-0.85,0.87,-0.44,-0.43,-1.10,0.40,-0.96,0.17]])
Y = np.array([[2.49,0.83,-0.25,3.10, 0.87, 0.02,-0.12,1.81,-0.83,0.43]])

XO = np.transpose(X)
Y = np.transpose(Y)

X = gradoX(XO,1)

w = regresionLineal(X,Y)

print w
YStar = np.dot(X,w)
mse = np.sum((YStar - Y)**2,axis=0)/float(Y.shape[0])
print('MSE: %.4f' %(mse))

xo = np.arange(-1.5, 1.5, 0.05)
x = xo.reshape((xo.shape[0],1))
x = gradoX(x,1)

fig, ax = plt.subplots()
ax.scatter(xo, np.dot(x,w))
ax.scatter(XO, Y, color='red')
plt.ylim((-0.5,3))

fig.show()

for grado in range(2,10):

    X = gradoX(XO,grado)
    w = regresionLineal(X,Y)
    
    print w
    YStar = np.dot(X,w)
    mse = np.sum((YStar - Y)**2,axis=0)/float(Y.shape[0])
    print('MSE: %.4f' %(mse))
    
    xo = np.arange(-1.5, 1.5, 0.005)
    x = xo.reshape((xo.shape[0],1))
    x = gradoX(x,grado)
    
    fig, ax = plt.subplots()
    ax.scatter(xo, np.dot(x,w))
    ax.scatter(XO, Y, color='red')
    plt.ylim((-0.5,3))

    fig.show()

