#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt

from sklearn.datasets import load_iris
from sklearn.naive_bayes import GaussianNB

# Parámetros
n_classes = 3
plot_colors = "bry" # Azul, rojo y amarillo
plot_step = 0.02

# Cargar los datos
iris = load_iris()

# Para cada par de variables
# enumerate devuelve tanto el elemento como el índice
for pairidx, pair in enumerate([[0, 1], [0, 2], [0, 3],
                                [1, 2], [1, 3], [2, 3]]):
    # Usamos solo esas dos variables
    X = iris.data[:, pair]
    y = iris.target

    # Entrenar Naive Bayes, suponiendo Gaussianas para las variables
    clf = GaussianNB()
    clf.fit(X, y)

    # Pintamos la superficie de decisión
    plt.subplot(2, 3, pairidx + 1)

    x_min, x_max = X[:, 0].min() - 1, X[:, 0].max() + 1
    y_min, y_max = X[:, 1].min() - 1, X[:, 1].max() + 1
    xx, yy = np.meshgrid(np.arange(x_min, x_max, plot_step),
                         np.arange(y_min, y_max, plot_step))

    Z = clf.predict(np.c_[xx.ravel(), yy.ravel()])
    Z = Z.reshape(xx.shape)
    cs = plt.contourf(xx, yy, Z, cmap=plt.cm.Paired)

    plt.xlabel(iris.feature_names[pair[0]])
    plt.ylabel(iris.feature_names[pair[1]])
    plt.axis("tight")

    # Pintamos los puntos de entrenamiento
    # zip devuelve una lista de tuplas formadas por los elementos
    # en orden de las dos listas que se le pasan
    for i, color in zip(range(n_classes), plot_colors):
        idx = np.where(y == i)
        plt.scatter(X[idx, 0], X[idx, 1], c=color, label=iris.target_names[i],
                    cmap=plt.cm.Paired)

    plt.axis("tight")

plt.suptitle("Superficies de Gaussian Naive Bayes sobre cada par de caracteristicas")
plt.legend()
plt.show()
