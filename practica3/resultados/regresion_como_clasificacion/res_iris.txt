==> iris/res_iris_r0.05_o1_e0.00001_l1.txt <==
*********************
MSE de entrenamiento: 0.061522 +- 0.013935
MSE de test: 0.044825 +- 0.007212
CCR de entrenamiento: 93.57% +- 1.18%
CCR de test: 94.21% +- 2.58%
Numero de coeficientes: 0.00 +- 0.00

==> iris/res_iris_r0.1_o1_e0.00001_l1.txt <==
*********************
MSE de entrenamiento: 0.029515 +- 0.001494
MSE de test: 0.029997 +- 0.001631
CCR de entrenamiento: 96.79% +- 0.44%
CCR de test: 95.79% +- 1.29%
Numero de coeficientes: 0.00 +- 0.00

==> iris/res_iris_r0.25_o1_e0.00001_l1.txt <==
*********************
MSE de entrenamiento: 0.021743 +- 0.001124
MSE de test: 0.033038 +- 0.006423
CCR de entrenamiento: 97.14% +- 0.67%
CCR de test: 97.89% +- 1.97%
Numero de coeficientes: 0.00 +- 0.00

==> iris/res_iris_r0.5_o1_e0.00001_l1.txt <==
*********************
MSE de entrenamiento: 0.013209 +- 0.000529
MSE de test: 0.035125 +- 0.003683
CCR de entrenamiento: 98.75% +- 0.44%
CCR de test: 95.79% +- 1.29%
Numero de coeficientes: 0.00 +- 0.00
