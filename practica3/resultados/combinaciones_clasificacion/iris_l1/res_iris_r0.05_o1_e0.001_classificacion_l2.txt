-----------
Semilla: 10
-----------
Número de RBFs utilizadas: 5
Coeficientes (sesgo al final):
[[ -8.9342263    5.7161963    7.39078089  -6.4030811   -4.39949245
   -0.4789607 ]
 [ 18.92759048  11.97416099  -1.20445142  12.85662913  -3.93562484
   -7.7084076 ]
 [ -6.30181717 -58.75628408 -30.12810403  -9.62308962   3.4109403
    6.41063322]]
MSE de entrenamiento: 0.025191
MSE de test: 0.027926
CCR de entrenamiento: 96.43%
CCR de test: 92.11%
Numero de coeficientes: 6
Matriz de confusion: 
[[13  0  0]
 [ 0  9  3]
 [ 0  0 13]]
-----------
Semilla: 20
-----------
Número de RBFs utilizadas: 5
Coeficientes (sesgo al final):
[[ -4.43954455   6.50076379   6.68953912  -6.22476164  -8.79287004
   -0.5640531 ]
 [ -3.36349801  11.100415    -1.37344997  11.93781624  17.94641511
   -7.32884838]
 [  2.48751426 -60.11350911 -30.53975058  -9.24159423  -4.92968279
    6.2828126 ]]
MSE de entrenamiento: 0.026133
MSE de test: 0.028946
CCR de entrenamiento: 96.43%
CCR de test: 92.11%
Numero de coeficientes: 6
Matriz de confusion: 
[[13  0  0]
 [ 0  9  3]
 [ 0  0 13]]
-----------
Semilla: 30
-----------
Número de RBFs utilizadas: 5
Coeficientes (sesgo al final):
[[ -6.4088111    5.05983988   7.84027589  -9.12955514  -4.56015629
   -0.38651318]
 [ 14.3089961   11.88620821   0.3259564   20.21246535  -5.12983181
   -8.22633186]
 [-10.87006067 -56.20669129 -30.58213847  -7.97296867   4.65591518
    6.89900888]]
MSE de entrenamiento: 0.023522
MSE de test: 0.025503
CCR de entrenamiento: 96.43%
CCR de test: 94.74%
Numero de coeficientes: 6
Matriz de confusion: 
[[13  0  0]
 [ 0 10  2]
 [ 0  0 13]]
-----------
Semilla: 40
-----------
Número de RBFs utilizadas: 5
Coeficientes (sesgo al final):
[[ -3.43445537 -11.94245091  10.58809955  -4.55534524  -4.59471568
   -0.55436196]
 [ -2.85444482  21.32979667  11.87940493  19.6830325   -7.75424989
   -8.77167393]
 [  1.7228834   -1.62925974 -62.94236489  -2.66809589  -8.58316196
    8.61618857]]
MSE de entrenamiento: 0.023100
MSE de test: 0.038328
CCR de entrenamiento: 95.54%
CCR de test: 89.47%
Numero de coeficientes: 6
Matriz de confusion: 
[[13  0  0]
 [ 0  8  4]
 [ 0  0 13]]
-----------
Semilla: 50
-----------
Número de RBFs utilizadas: 5
Coeficientes (sesgo al final):
[[-11.61281602  -6.92056089  -2.52211346  -2.90218971  10.79058597
   -0.70713861]
 [  8.89749025   2.46976657  14.33162558 -12.68857668   0.81478903
   -2.86395903]
 [  2.63715607  -0.41878063 -10.79089127   9.07101074 -36.37828675
    2.79474455]]
MSE de entrenamiento: 0.015701
MSE de test: 0.007702
CCR de entrenamiento: 96.43%
CCR de test: 97.37%
Numero de coeficientes: 6
Matriz de confusion: 
[[13  0  0]
 [ 0 11  1]
 [ 0  0 13]]
*********************
Resumen de resultados
*********************
MSE de entrenamiento: 0.022729 +- 0.003683
MSE de test: 0.025681 +- 0.009989
CCR de entrenamiento: 96.25% +- 0.36%
CCR de test: 93.16% +- 2.68%
Numero de coeficientes: 6.00 +- 0.00
CCR de entrenamiento: 96.25% +- 0.36%
CCR de test: 93.16% +- 2.68%
