-----------
Semilla: 10
-----------
Número de RBFs utilizadas: 5
Coeficientes (sesgo al final):
[[ -13.98350347    9.26786073   12.05160524   -9.08009332   -8.27719031
    -0.68773144]
 [  20.90041909   14.12771161   -1.81124118   15.06058892   -4.90421563
    -8.55711974]
 [ -25.74217503 -215.04818391   30.88998097  -31.60379943   18.9883826
    14.94218815]]
MSE de entrenamiento: 0.018134
MSE de test: 0.030993
CCR de entrenamiento: 97.32%
CCR de test: 92.11%
Numero de coeficientes: 6
Matriz de confusion: 
[[13  0  0]
 [ 0  9  3]
 [ 0  0 13]]
-----------
Semilla: 20
-----------
Número de RBFs utilizadas: 5
Coeficientes (sesgo al final):
[[  -8.07964835   10.63450175   10.72648018   -8.74162839  -13.90255454
    -0.84421629]
 [  -4.25578053   13.30718525   -2.28171128   13.9194428    19.72125548
    -8.07719843]
 [  18.8198728  -242.67790973   47.67499932  -33.04051664  -25.13271752
    15.48801655]]
MSE de entrenamiento: 0.018578
MSE de test: 0.032969
CCR de entrenamiento: 97.32%
CCR de test: 92.11%
Numero de coeficientes: 6
Matriz de confusion: 
[[13  0  0]
 [ 0  9  3]
 [ 0  0 13]]
-----------
Semilla: 30
-----------
Número de RBFs utilizadas: 5
Coeficientes (sesgo al final):
[[ -9.22527499e+00   8.31809553e+00   1.27457244e+01  -1.44722419e+01
   -8.59976347e+00  -4.85534672e-01]
 [  1.66162971e+01   1.38194128e+01   1.69403276e-01   2.22678018e+01
   -6.11699323e+00  -9.14312321e+00]
 [ -3.00919570e+01  -2.06967723e+02   2.20862460e+01  -2.50862498e+01
    1.99740389e+01   1.43332187e+01]]
MSE de entrenamiento: 0.017538
MSE de test: 0.025976
CCR de entrenamiento: 97.32%
CCR de test: 94.74%
Numero de coeficientes: 6
Matriz de confusion: 
[[13  0  0]
 [ 0 10  2]
 [ 0  0 13]]
-----------
Semilla: 40
-----------
Número de RBFs utilizadas: 5
Coeficientes (sesgo al final):
[[  -7.29666784  -19.13721049   16.66279924   -6.99157506   -6.94347036
    -0.72708963]
 [  -3.9585161    24.58072739   14.05963488   23.78710701   -9.52435158
   -10.02062664]
 [  12.27115503  -17.30161342 -171.29865697  -23.81951354  -23.34180018
    27.25312752]]
MSE de entrenamiento: 0.018956
MSE de test: 0.032677
CCR de entrenamiento: 95.54%
CCR de test: 89.47%
Numero de coeficientes: 6
Matriz de confusion: 
[[13  0  0]
 [ 0  8  4]
 [ 0  0 13]]
-----------
Semilla: 50
-----------
Número de RBFs utilizadas: 5
Coeficientes (sesgo al final):
[[ -20.64532735   -9.85116685   -3.67944333   -3.6511783    17.82723924
    -1.00048245]
 [   9.25565559    2.9218496    14.8566742   -13.20154611    1.18680796
    -3.06435742]
 [ -12.21371685  -39.14735928    4.48405105    4.49511741 -188.24319173
    22.92562086]]
MSE de entrenamiento: 0.014343
MSE de test: 0.011279
CCR de entrenamiento: 96.43%
CCR de test: 97.37%
Numero de coeficientes: 6
Matriz de confusion: 
[[13  0  0]
 [ 0 11  1]
 [ 0  0 13]]
*********************
Resumen de resultados
*********************
MSE de entrenamiento: 0.017510 +- 0.001652
MSE de test: 0.026779 +- 0.008146
CCR de entrenamiento: 96.79% +- 0.71%
CCR de test: 93.16% +- 2.68%
Numero de coeficientes: 6.00 +- 0.00
CCR de entrenamiento: 96.79% +- 0.71%
CCR de test: 93.16% +- 2.68%
