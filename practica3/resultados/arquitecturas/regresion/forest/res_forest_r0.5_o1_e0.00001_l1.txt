-----------
Semilla: 10
-----------
Número de RBFs utilizadas: 193
MSE de entrenamiento: 0.001099
MSE de test: 0.009661
-----------
Semilla: 20
-----------
Número de RBFs utilizadas: 193
MSE de entrenamiento: 0.000786
MSE de test: 0.010248
-----------
Semilla: 30
-----------
Número de RBFs utilizadas: 193
MSE de entrenamiento: 0.001065
MSE de test: 0.009821
-----------
Semilla: 40
-----------
Número de RBFs utilizadas: 193
MSE de entrenamiento: 0.001012
MSE de test: 0.009887
-----------
Semilla: 50
-----------
Número de RBFs utilizadas: 193
MSE de entrenamiento: 0.001024
MSE de test: 0.009665
*********************
Resumen de resultados
*********************
MSE de entrenamiento: 0.000997 +- 0.000110
MSE de test: 0.009857 +- 0.000215
