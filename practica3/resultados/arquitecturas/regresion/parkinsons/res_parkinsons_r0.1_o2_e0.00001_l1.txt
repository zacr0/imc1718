-----------
Semilla: 10
-----------
Número de RBFs utilizadas: 440
MSE de entrenamiento: 0.016767
MSE de test: 0.021793
-----------
Semilla: 20
-----------
Número de RBFs utilizadas: 440
MSE de entrenamiento: 0.016947
MSE de test: 0.021276
-----------
Semilla: 30
-----------
Número de RBFs utilizadas: 440
MSE de entrenamiento: 0.016513
MSE de test: 0.020791
-----------
Semilla: 40
-----------
Número de RBFs utilizadas: 440
MSE de entrenamiento: 0.017073
MSE de test: 0.020825
-----------
Semilla: 50
-----------
Número de RBFs utilizadas: 440
MSE de entrenamiento: 0.016591
MSE de test: 0.021078
*********************
Resumen de resultados
*********************
MSE de entrenamiento: 0.016778 +- 0.000210
MSE de test: 0.021153 +- 0.000366
