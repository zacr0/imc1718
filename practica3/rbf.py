#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 28 12:37:04 2016

@author: pagutierrez
"""

# Librerias
import pandas as pd
import numpy as np
import click

from sklearn import preprocessing                    # Preprocesamiento
from sklearn.metrics import confusion_matrix         # Matriz de confusion
from sklearn.metrics import mean_squared_error       # MSE
from sklearn.metrics import accuracy_score           # CCR (regresion como clasicacion)
from sklearn.cluster import KMeans                   # KMeans
from sklearn.linear_model import LogisticRegression  # Regresion Logistica
# COMPATIBLIDAD SERVIDOR UCO, comentar la primera y descomentar la segunda:
from sklearn.model_selection import StratifiedShuffleSplit # Particionamiento estratificado
#from sklearn.cross_validation import StratifiedShuffleSplit # Particionamiento estratificado
from sklearn.metrics.pairwise import pairwise_distances # Matriz de distancias

# Parametros
@click.command()
@click.option('--train_file', '-t', default=None, required=True,
              help=u'Fichero con los datos de entrenamiento.')
@click.option('--test-file', '-T', default=None, 
              help=u'Fichero con los datos de test. Si no se especifica, se'
              'utilizara el fichero de entrenamiento como conjunto de test.')
@click.option('--classification', '-c', is_flag=True, default=False,
              help=u'Indica si el problema es de clasificacion (True) o de ' 
              'regresion (False). Por defecto, se supone que el problema es'
              ' de regresion.')
@click.option('--ratio_rbf', '-r', default=0.1, help=u'Indica la razon (en tanto'
            'por uno) de neuronas RBF con respecto al total de patrones de'
            'entrenamiento.')
@click.option('--l2', '-l', is_flag=True, default=False, help=u'Indica si se'
              ' emplea la regularizacion L2 en lugar de la regularizacion L1.')
@click.option('--eta', '-e', default=1e-2, help=u'Indica el valor del parametro'
              'eta, o tasa de aprendizaje.')
@click.option('--outputs', '-o', default=1, help=u'Indica el numero de columnas'
              'de salida que tiene el conjunto de datos.')

def entrenar_rbf_total(train_file, test_file, classification, ratio_rbf, l2, eta, outputs):
    """ Modelo de aprendizaje supervisado mediante red neuronal de tipo RBF.
        Ejecución de 5 semillas.
    """
    train_mses = np.empty(5)
    train_ccrs = np.empty(5)
    test_mses = np.empty(5)
    test_ccrs = np.empty(5)
    test_coefs = np.empty(5)
    
    for s in range(10,60,10):   
        print("-----------")
        print("Semilla: %d" % s)
        print("-----------")     
        np.random.seed(s)
        train_mses[s//10-1], test_mses[s//10-1], train_ccrs[s//10-1], test_ccrs[s//10-1], test_coefs[s//10-1], mat_conf = \
            entrenar_rbf(train_file, test_file, classification, ratio_rbf, l2, eta, outputs)
        print("MSE de entrenamiento: %f" % train_mses[s//10-1])
        print("MSE de test: %f" % test_mses[s//10-1])
        if classification:
            print("CCR de entrenamiento: %.2f%%" % train_ccrs[s//10-1])
            print("CCR de test: %.2f%%" % test_ccrs[s//10-1])
            print("Numero de coeficientes: %d" % test_coefs[s//10-1])
            print("Matriz de confusion: ")
            print(mat_conf)
    
    print("*********************")
    print("Resumen de resultados")
    print("*********************")
    print("MSE de entrenamiento: %f +- %f" % (np.mean(train_mses), np.std(train_mses)))
    print("MSE de test: %f +- %f" % (np.mean(test_mses), np.std(test_mses)))
    
    if classification:
        print("CCR de entrenamiento: %.2f%% +- %.2f%%" % (np.mean(train_ccrs), np.std(train_ccrs)))
        print("CCR de test: %.2f%% +- %.2f%%" % (np.mean(test_ccrs), np.std(test_ccrs)))
        print("Numero de coeficientes: %.2f +- %.2f" % (np.mean(test_coefs), np.std(test_coefs)))
        
    # CCR (regresion como clasificacion, comentar en caso de ser necesario)
    print("CCR de entrenamiento: %.2f%% +- %.2f%%" % (np.mean(train_ccrs), np.std(train_ccrs)))
    print("CCR de test: %.2f%% +- %.2f%%" % (np.mean(test_ccrs), np.std(test_ccrs)))

def entrenar_rbf(train_file, test_file, classification, ratio_rbf, l2, eta, outputs):
    """ Modelo de aprendizaje supervisado mediante red neuronal de tipo RBF.
        Una única ejecución.
        Recibe los siguientes parámetros:
            - train_file: nombre del fichero de entrenamiento.
            - test_file: nombre del fichero de test.
            - classification: True si el problema es de clasificacion.
            - ratio_rbf: Ratio (en tanto por uno) de neuronas RBF con 
              respecto al total de patrones.
            - l2: True si queremos utilizar L2 para la Regresión Logística. 
              False si queremos usar L1 (para regresión logística).
            - eta: valor del parámetro de regularización para la Regresión 
              Logística.
            - outputs: número de variables que se tomarán como salidas 
              (todas al final de la matriz).
        Devuelve:
            - train_mse: Error de tipo Mean Squared Error en entrenamiento. 
              En el caso de clasificación, calcularemos el MSE de las 
              probabilidades predichas frente a las objetivo.
            - test_mse: Error de tipo Mean Squared Error en test. 
              En el caso de clasificación, calcularemos el MSE de las 
              probabilidades predichas frente a las objetivo.
            - train_ccr: Error de clasificación en entrenamiento. 
              En el caso de regresión, devolvemos un cero.
            - test_ccr: Error de clasificación en test. 
              En el caso de regresión, devolvemos un cero.
    """
    train_inputs, train_outputs, test_inputs, test_outputs = lectura_datos(train_file, 
                                                                           test_file,
                                                                           outputs)

    # Obtener num_rbf a partir de ratio_rbf
    num_rbf = int(train_inputs.shape[0] * ratio_rbf)
    print("Número de RBFs utilizadas: %d" %(num_rbf))
    
    kmedias, distancias, centros = clustering(classification, train_inputs, 
                                              train_outputs, num_rbf)
    
    # Radios de las RBFs
    radios = calcular_radios(centros, num_rbf)
    
    # Matriz R de entrenamiento (salidas de entrenamiento)
    matriz_r = calcular_matriz_r(distancias, radios)

    # Ajuste de pesos de capa de salida
    if not classification:
        # Regresion: aplicar la pseudo-inversa para obtener matriz Beta (matriz
        # de coeficientes)
        coeficientes = invertir_matriz_regresion(matriz_r, train_outputs)
    else:
        # Clasificacion: aplicar regresion logistica
        # Nota: pasamos train_outputs asi para evitar un warning de conversion
        # de datos
        logreg = logreg_clasificacion(matriz_r, np.ravel(train_outputs), eta, l2)
    
    # Distancias de los centroides a los patrones de test
    distancias_test = kmedias.transform(test_inputs)
    
    # Matriz R de test (salidas de test)
    matriz_r_test = calcular_matriz_r(distancias_test, radios)

    if not classification:
        """
        Obtener las predicciones de entrenamiento y de test y calcular el MSE
        """
        # Problemas de Regresion
        # Prediccion y error en entrenamiento
        train_pred = np.dot(matriz_r, coeficientes)
        train_mse = mean_squared_error(train_outputs, train_pred)
        
        # Prediccion y error en test
        test_pred = np.dot(matriz_r_test, coeficientes)
        test_mse = mean_squared_error(test_outputs, test_pred)
        
        # Clasificacion como regresion (descomentar para usarlo)
        # Redondeo de salidas obtenidas al entero mas cercano
        train_pred_round = np.round(train_pred)
        test_pred_round = np.round(test_pred)
        
        # Calculo de CCR comparando con salidas esperadas
        train_ccr = accuracy_score(train_outputs, train_pred_round) * 100
        test_ccr = accuracy_score(test_outputs, test_pred_round) * 100
        
        # Valores por defecto de variables de salida no utilizadas
        # Comentar CCR si se utiliza clasificacion como regresion
        #train_ccr = 0
        #test_ccr = 0
        num_coefs = 0
        mat_conf = []
        
    else:
        """
        Obtener las predicciones de entrenamiento y de test y calcular
        el CCR. Calcular también el MSE, comparando las probabilidades 
        obtenidas y las probabilidades objetivo
        """
        # Problemas de Clasificacion
        # MSE
        # Binarizacion de las salidas deseadas (etiquetas de clase)
        train_outputs_bin = preprocessing.label_binarize(y=train_outputs, classes=logreg.classes_)
        test_outputs_bin = preprocessing.label_binarize(y=test_outputs, classes=logreg.classes_)
        
        # Extraccion de probabilidades de pertenencia a cada clase predicha
        train_pred_prob = logreg.predict_proba(matriz_r)        
        test_pred_prob = logreg.predict_proba(matriz_r_test)
        
        # Calculo del MSE, comparando salidas probabilidadas obtenidas con las
        # probabilidades deseadas
        train_mse = mean_squared_error(train_outputs_bin, train_pred_prob)
        test_mse = mean_squared_error(test_outputs_bin, test_pred_prob)
        
        
        # CCR
        train_ccr = logreg.score(matriz_r, train_outputs) * 100
        test_ccr = logreg.score(matriz_r_test, test_outputs) * 100
        
        # Numero de coeficientes del modelo de regresion (para comparacion de 
        # tipos de regularizacion)
        coefs = np.absolute(logreg.coef_)[0] # coeficientes en valor absoluto
        num_coefs = np.count_nonzero(coefs > 0.00001) # nulos < 10^-5
        print("Coeficientes (sesgo al final):")
        print(logreg.coef_)        
        
        
        # Matriz de confusion de test
        test_pred = logreg.predict(matriz_r_test)
        mat_conf = confusion_matrix(test_outputs, test_pred, logreg.classes_) 
        
    return train_mse, test_mse, train_ccr, test_ccr, num_coefs, mat_conf

    
def lectura_datos(fichero_train, fichero_test, outputs):
    """ Realiza la lectura de datos.
        Recibe los siguientes parámetros:
            - fichero_train: nombre del fichero de entrenamiento.
            - fichero_test: nombre del fichero de test.
            - outputs: número de variables que se tomarán como salidas 
              (todas al final de la matriz).
        Devuelve:
            - train_inputs: matriz con las variables de entrada de 
              entrenamiento.
            - train_outputs: matriz con las variables de salida de 
              entrenamiento.
            - test_inputs: matriz con las variables de entrada de 
              test.
            - test_outputs: matriz con las variables de salida de 
              test.
    """
    # Asignar fichero de train como test por defecto
    if not fichero_test:
        fichero_test = fichero_train
    
    # Lectura de datos
    train_data = pd.read_csv(fichero_train, header=None)
    test_data = pd.read_csv(fichero_test, header=None)
    
    # Seleccion de variables de entrada
    train_inputs = train_data.values[:, 0:-outputs]
    test_inputs = test_data.values[:, 0:-outputs]
    
    # Seleccion de variables de salida
    train_outputs = train_data.values[:, -outputs:]
    test_outputs = test_data.values[:, -outputs:]

    return train_inputs, train_outputs, test_inputs, test_outputs

def inicializar_centroides_clas(train_inputs, train_outputs, num_rbf):
    """ Inicializa los centroides para el caso de clasificación.
        Debe elegir, aprox., num_rbf/num_clases
        patrones por cada clase. Recibe los siguientes parámetros:
            - train_inputs: matriz con las variables de entrada de 
              entrenamiento.
            - train_outputs: matriz con las variables de salida de 
              entrenamiento.
            - num_rbf: número de neuronas de tipo RBF.
        Devuelve:
            - centroides: matriz con todos los centroides iniciales
                          (num_rbf x num_entradas).
    """
    
    # Configuracion de generador de conjuntos estratificados
    sss = StratifiedShuffleSplit(n_splits=1, train_size=num_rbf, test_size=None)
    gen_splits = sss.split(train_inputs, train_outputs)
    
    # Extraccion de patrones del conjunto estratificado, a partir de sus indices
    indices = list(gen_splits)[0][0] # seleccionamos unicamente la parte de train
    centroides = train_inputs[indices]
    
    return centroides

def clustering(clasificacion, train_inputs, train_outputs, num_rbf):
    """ Realiza el proceso de clustering. En el caso de la clasificación, se
        deben escoger los centroides usando inicializar_centroides_clas()
        En el caso de la regresión, se escogen aleatoriamente.
        Recibe los siguientes parámetros:
            - clasificacion: True si el problema es de clasificacion.
            - train_inputs: matriz con las variables de entrada de 
              entrenamiento.
            - train_outputs: matriz con las variables de salida de 
              entrenamiento.
            - num_rbf: número de neuronas de tipo RBF.
        Devuelve:
            - kmedias: objeto de tipo sklearn.cluster.KMeans ya entrenado.
            - distancias: matriz (num_patrones x num_rbf) con la distancia 
              desde cada patrón hasta cada rbf.
            - centros: matriz (num_rbf x num_entradas) con los centroides 
              obtenidos tras el proceso de clustering.
    """
    
    # Inicializacion de centroides
    if clasificacion:
        # Problema de clasificacion: escoger numero_de_clusters / numero_de_clases patrones 
        # de cada clase.
        centroides = inicializar_centroides_clas(train_inputs, train_outputs, num_rbf)
        kmedias = KMeans(n_clusters=num_rbf, init=centroides, n_init=1, max_iter=500, n_jobs=-1)
    else:
        # Problema de regresion: seleccionar aleatoriamente numero_de_clusters patrones.
        kmedias = KMeans(n_clusters=num_rbf, init='random', n_init=1, max_iter=500, n_jobs=-1)
        
    # Calculo de matriz de distancias de cada patron hasta cada rbf
    distancias = kmedias.fit_transform(train_inputs)
    
    # Extraccion de centros obtenidos
    centros = kmedias.cluster_centers_
    
    return kmedias, distancias, centros

def calcular_radios(centros, num_rbf):
    """ Calcula el valor de los radios tras el clustering.
        Recibe los siguientes parámetros:
            - centros: conjunto de centroides.
            - num_rbf: número de neuronas de tipo RBF.
        Devuelve:
            - radios: vector (num_rbf) con el radio de cada RBF.
    """

    # Calculo de matriz de distancias
    mat_dist = pairwise_distances(centros, n_jobs=-1)
    
    # Division suma de distancias de cada centro (por filas)
    radios = mat_dist.sum(axis=1) / (2*(num_rbf-1))
    
    return radios

def calcular_matriz_r(distancias, radios):
    """ Devuelve el valor de activación de cada neurona para cada patrón 
        (matriz R en la presentación)
        Recibe los siguientes parámetros:
            - distancias: matriz (num_patrones x num_rbf) con la distancia 
              desde cada patrón hasta cada rbf.
            - radios: array (num_rbf) con el radio de cada RBF.
        Devuelve:
            - matriz_r: matriz (num_patrones x (num_rbf+1)) con el valor de 
              activación (out) de cada RBF para cada patrón. Además, añadimos
              al final, en la última columna, un vector con todos los 
              valores a 1, que actuará como sesgo.
    """
    # Calculo de salidas de cada RBF mediante funcion gaussiana
    matriz_r = np.exp( -np.square(distancias) / (2*np.square(radios)) )
    
    # Agregar vector columna unitario a la matriz
    col_sesgo = np.ones((matriz_r.shape[0],1))
    matriz_r = np.append(matriz_r, col_sesgo, axis=1)
    
    return matriz_r

def invertir_matriz_regresion(matriz_r, train_outputs):
    """ Devuelve el vector de coeficientes obtenidos para el caso de la 
        regresión (matriz beta en las diapositivas)
        Recibe los siguientes parámetros:
            - matriz_r: matriz (num_patrones x (num_rbf+1)) con el valor de 
              activación (out) de cada RBF para cada patrón. Además, añadimos
              al final, en la última columna, un vector con todos los 
              valores a 1, que actuará como sesgo.
            - train_outputs: matriz con las variables de salida de 
              entrenamiento.
        Devuelve:
            - coeficientes: vector (num_rbf+1) con el valor del sesgo y del 
              coeficiente de salida para cada rbf.
    """
    # Calculo manual de la Pseudo-inversa de Moore-Penrose
    # NOTA
    # Este caso no reporta buenos resultados debido a la posible presencia de 
    # dependencias lineales en los valores de la matriz R, por lo que se opta por
    # emplear la funcion ya implementada en numpy.
    # Los malos resultados pueden observarse, como ejemplo, realizando una
    # ejecucion del script sobre la base de datos forest con r = 0.5 y e = 0.00001
    #matriz_r_trans = np.matrix.transpose(matriz_r) # Rt
    #pseudo_inversa = np.dot(matriz_r_trans, matriz_r) # Rt x R
    #pseudo_inversa = np.linalg.inv(pseudo_inversa) # (Rt x R)^-1
    #pseudo_inversa = np.dot(pseudo_inversa, matriz_r_trans) #  (Rt x R)^-1 x Rt
    
    # Aplicacion de caso general: calculo de pseudo-inversa de Moore-Penrose
    # Esta funcion elimina el problema indicado anteriormente
    pseudo_inversa = np.linalg.pinv(matriz_r)
    
    # Calculo de vector de coeficentes (beta transpuesta), obtenemos un parametro
    # por cada salida + sesgo
    coeficientes = np.dot(pseudo_inversa, train_outputs)
    
    return coeficientes

def logreg_clasificacion(matriz_r, train_outputs, eta, l2):
    """ Devuelve el objeto de tipo regresión logística obtenido a partir de la
        matriz R.
        Recibe los siguientes parámetros:
            - matriz_r: matriz (num_patrones x (num_rbf+1)) con el valor de 
              activación (out) de cada RBF para cada patrón. Además, añadimos
              al final, en la última columna, un vector con todos los 
              valores a 1, que actuará como sesgo.
            - train_outputs: matriz con las variables de salida de 
              entrenamiento.
            - eta: valor del parámetro de regularización para la Regresión 
              Logística.
            - l2: True si queremos utilizar L2 para la Regresión Logística. 
              False si queremos usar L1.
        Devuelve:
            - logreg: objeto de tipo sklearn.linear_model.LogisticRegression ya
              entrenado.
    """
    # Determinar tipo de regularizacion a utilizar (l1 por defecto)
    tipo_regularizacion = 'l1'
    if l2:
        tipo_regularizacion = 'l2'
   
    # C (importancia del error de aproximacion frente al de regularizacion)
    # A menor C, mayor la regularizacion
    c = 1 / eta
    
    # Entrenamiento de modelo de regresion logistica (algoritmo liblinear por defecto)
    # Se deshabilita la insercion de sesgo en la funcion con fit_intercept=False
    #logreg = LogisticRegression(penalty=tipo_regularizacion, C=c, fit_intercept=False)
    logreg = LogisticRegression(penalty=tipo_regularizacion, C=c)
    logreg = logreg.fit(matriz_r, train_outputs)
    
    return logreg


if __name__ == "__main__":
    entrenar_rbf_total()
