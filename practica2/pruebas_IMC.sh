#!/bin/bash

#######SCRIPT creado por David Serrano Gemes ##########
#GIT: 	https://github.com/davidserranogemes
#		https://gitlab.com/hidesagred97

#NUM_IT es el numero de iteraciones que hara la red neuronal
#FILE_ROUTE es donde estan las bases de datos
#ARQUITECTURA es la ruta de la arquitectura a usa por defecto, incluyendo los parametros
#EXE: ruta del ejecutable
#DIR es la ruta donde se guardaran los resultados. TODOS se vuelcan en ficheros, cada ejecucion en el suyo

#El programa funciona sin argumentos usando todos los train de FILE_ROUTE y los parametros de arquitectura, haciendo todas las posibles combinaciones entre ellos

#El primer parametro opcional del programa es el nombre de un fichero especifico en FILE_ROUTE, por ejemplo forest. Eso hace que solo se ejecute con la base de datos FOREST
#El segundo parametro opcional del programa es el nombre del fichero de arquitectura. La ruta comienza en la carpeta dodne este el script

#Se genera una carpeta resultados, con la hora de inicio de ejecucion  y una estructura de carpetas segun la base de datos. Se crea un fichero de log resumen, que indica el tiempo que se ha tardado y cuantas ejecuciones se han hecho

#Fichero de arquitecturas 
#H: marca las neuronas por capa
#L: marca el numero de capas.
#E: factor de aprendizaje
#M: momento
#V: validacion
#D: decremento
#Se hacen todas las combinaciones posibles entre los parametros


#NUM_IT='1000'
FILE_ROUTE='./basesDatosPr2IMC/dat/'
ARQUITECTURA='./arquitecturas.txt'
#EXE='./Release/practica2'
EXE='./practica2/Debug/practica2'

DATE=`date '+%Y-%m-%d_%H:%M'`
DIR='./resultados/resultados_'$DATE

mkdir $DIR

#Hacen make a los ficheros, asegurando que este actualizados, pueden comentarse si ya los tienes compilados para poder acelerar el programa un poco
#cd Release
#make clean
#make
#cd ..



FILES=$(ls $FILE_ROUTE | grep "test" | sed -r 's/^(test_)(.*)(\.dat)/\2/')

if [ $# = 1 ]
	then
	FILES=$1
else if [ $# = 2 ]
	 then
	 	FILES=$1
	 	ARQUITECTURA=$2
	 fi
fi
echo "Inicio de las ejecuciones: `date '+%Y-%m-%d_%H:%M:%S'`" > $DIR/log.txt
for f in $FILES
do
	mkdir $DIR/$f
	echo "	Inicio de las ejecuciones de $f: `date '+%Y-%m-%d_%H:%M:%S'`" >> $DIR/log.txt
    
    for i in $( cat $ARQUITECTURA | grep '^I:' | sed -r 's/^(I\:)(.*)/\2/')
    do
	    for h in $( cat $ARQUITECTURA | grep '^H:' | sed -r 's/^(H\:)(.*)/\2/')
	    do
		    for l in $( cat $ARQUITECTURA | grep '^L:' | sed -r 's/^(L\:)(.*)/\2/')
		    do
			    for e in $( cat $ARQUITECTURA | grep '^E:' | sed -r 's/^(E\:)(.*)/\2/')
			    do
				    for m in $( cat $ARQUITECTURA | grep '^M:' | sed -r 's/^(M\:)(.*)/\2/')
				    do
					    for v in $( cat $ARQUITECTURA | grep '^V:' | sed -r 's/^(V\:)(.*)/\2/')
					    do
						    for d in $( cat $ARQUITECTURA | grep '^D:' | sed -r 's/^(D\:)(.*)/\2/')
						    do
							    for fe in $( cat $ARQUITECTURA | grep '^F:' | sed -r 's/^(F\:)(.*)/\2/')
							    do
								    for NUM_IT in $( cat $ARQUITECTURA | grep '^I:' | sed -r 's/^(I\:)(.*)/\2/')
								    do
									    ONLINE=$(cat $ARQUITECTURA | grep '^O:' | sed -r 's/^(O\:)(.*)/\2/')
									    SOFTMAX=$(cat $ARQUITECTURA | grep '^S:' | sed -r 's/^(S\:)(.*)/\2/')
									    if [ $ONLINE = 1 ]; then
										    if [[ $SOFTMAX = 1 ]]; then
											    $EXE -t ./basesDatosPr2IMC/dat/train_$f.dat -T ./basesDatosPr2IMC/dat/test_$f.dat -i $i -h $h -l $l -e $e -m $m -v $v -d $d -f $fe -o -s > $DIR/$f/res_$f\_h$h\_l$l\_e$e\_m$m\_v$v\_d$d\_f$fe\_o\_s.txt
											    echo "		h:$h l:$l e:$e m:$m v:$v d:$d f: $fe o s : `date '+%Y-%m-%d_%H:%M:%S'`" >> $DIR/log.txt
											    echo "	 $f	h:$h l:$l e:$e m:$m v:$v d:$d f: $fe o s : `date '+%Y-%m-%d_%H:%M:%S'`" 
										    else
											    $EXE -t ./basesDatosPr2IMC/dat/train_$f.dat -T ./basesDatosPr2IMC/dat/test_$f.dat -i $i -h $h -l $l -e $e -m $m -v $v -d $d -f $fe -o > $DIR/$f/res_$f\_h$h\_l$l\_e$e\_m$m\_v$v\_d$d\_f$fe\_o.txt
											    echo "		h:$h l:$l e:$e m:$m v:$v d:$d f: $fe o : `date '+%Y-%m-%d_%H:%M:%S'`" >> $DIR/log.txt
											    echo "	 $f	h:$h l:$l e:$e m:$m v:$v d:$d f: $fe o : `date '+%Y-%m-%d_%H:%M:%S'`" 											
										    fi

									    else
										    if [[ $SOFTMAX = 1 ]]; then
											    $EXE -t ./basesDatosPr2IMC/dat/train_$f.dat -T ./basesDatosPr2IMC/dat/test_$f.dat -i $i -h $h -l $l -e $e -m $m -v $v -d $d -f $fe -s > $DIR/$f/res_$f\_h$h\_l$l\_e$e\_m$m\_v$v\_d$d\_f$fe\_s.txt
											    echo "		h:$h l:$l e:$e m:$m v:$v d:$d f: $fe s : `date '+%Y-%m-%d_%H:%M:%S'`" >> $DIR/log.txt
											    echo "	 $f	h:$h l:$l e:$e m:$m v:$v d:$d f: $fe s : `date '+%Y-%m-%d_%H:%M:%S'`" 
										    else
											    $EXE -t ./basesDatosPr2IMC/dat/train_$f.dat -T ./basesDatosPr2IMC/dat/test_$f.dat -i $i -h $h -l $l -e $e -m $m -v $v -d $d -f $fe > $DIR/$f/res_$f\_h$h\_l$l\_e$e\_m$m\_v$v\_d$d\_f$fe.txt
											    echo "		h:$h l:$l e:$e m:$m v:$v d:$d f: $fe : `date '+%Y-%m-%d_%H:%M:%S'`" >> $DIR/log.txt
											    echo "	 $f	h:$h l:$l e:$e m:$m v:$v d:$d f: $fe : `date '+%Y-%m-%d_%H:%M:%S'`"  											
										    fi
									    fi
									
								    done
							    done
						    done
					    done
				    done
			    done
		    done
	    done
    done
	echo "	Fin de las ejecuciones de $f: `date '+%Y-%m-%d_%H:%M:%S'`" >> $DIR/log.txt
done
echo "Fin de las ejecuciones `date '+%Y-%m-%d_%H:%M:%S'`" >> $DIR/log.txt

