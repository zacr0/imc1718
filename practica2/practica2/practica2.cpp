//============================================================================
// Introducción a los Modelos Computacionales
// Name        : practica1.cpp
// Author      : Pedro A. Gutiérrez
// Version     :
// Copyright   : Universidad de Córdoba
//============================================================================


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <iostream>
#include <ctime>    // Para cojer la hora time()
#include <cstdlib>  // Para establecer la semilla srand() y generar números aleatorios rand()
#include <string.h>
#include <math.h>
#include "imc/PerceptronMulticapa.h"
#include "imc/CmdLineParser.cpp"

using namespace imc;
using namespace std;

int main(int argc, char **argv) {
	// Control de argumentos indispensables
	if (argc < 3) {
		cerr << "Error: Numero de argumentos insuficiente." << endl;
		return EXIT_FAILURE;
	}

	// Procesar los argumentos de la línea de comandos
	CmdLineParser cml(argc,argv); 	// parser de linea de comandos
	string fichTrain,
		   fichTest;
	int nIteraciones = 1000,
		nCapas = 1,
		nNeuronas = 5,
		tipoError = 0;
	double eta = 0.7,
		mu = 1.0,
		ratioValidacion = 0.,
		factorDecremento = 1;
	bool bOnline = false,
		usarSoftmax = false;

	// Fichero datos de Training
	if (cml["-t"]) {
		fichTrain = cml("-t");
	} else {
		cerr << "Error: introduzca la ruta del fichero de entrenamiento (parametro -t)." << endl;
		return EXIT_FAILURE;
	}
	// Fichero datos de Test
	if (cml["-T"]) {
		fichTest = cml("-T");
	} else {
		fichTest = fichTrain;
		cout << "Asumiendo valor por defecto (" << fichTest << ") para la ruta del fichero de test (parametro -T)." << endl;
	}
	// Numero de iteraciones del bucle externo
	if (cml["-i"]) {
		nIteraciones = atoi( cml("-i").c_str() );
		if (nIteraciones <= 0) {
			cerr << "Error: introduzca un numero de iteraciones correcto (parametro -i)." << endl;
			return EXIT_FAILURE;
		}
	} else {
		cout << "Asumiendo valor por defecto (" << nIteraciones << ") para el numero de iteraciones (parametro -i)." << endl;
	}
	// Numero de capas ocultas
	if (cml["-l"]) {
		nCapas = atoi( cml("-l").c_str() );
		if (nCapas < 1) {
			cerr << "Error: introduzca un numero de capas valido (parametro -l)." << endl;
			return EXIT_FAILURE;
		}
	} else {
		cout << "Asumiendo valor por defecto (" << nCapas << ") para el numero de capas ocultas (parametro -l)." << endl;
	}
	// Numero de neuronas por capa
	if (cml["-h"]) {
		nNeuronas = atoi( cml("-h").c_str() ) ;
		if (nNeuronas <= 0) {
			cerr << "Error: introduzca un numero de neuronas valido (parametro -h)." << endl;
			return EXIT_FAILURE;
		}
	} else {
		cout << "Asumiendo valor por defecto (" << nNeuronas << ") para el numero de neuronas por capa (parametro -h)." << endl;
	}
	// eta (coeficiente de aprendizaje)
	if (cml["-e"]) {
		eta = atof( cml("-e").c_str() );
		if (eta <= 0) {
			cerr << "Error: introduzca un valor de eta valido (parametro -e)." << endl;
			return EXIT_FAILURE;
		}
	} else {
		cout << "Asumiendo valor por defecto (" << eta << ") para el valor de eta (parametro -e)." << endl;
	}
	// mu (coeficiente de momento)
	if (cml["-m"]) {
		mu = atof( cml("-m").c_str() );
		if (mu < 0) {
			cerr << "Error: introduzca un valor de mu valido (parametro -m)." << endl;
			return EXIT_FAILURE;
		}
	} else {
		cout << "Asumiendo valor por defecto (" << mu << ") para el valor de mu (parametro -m)." << endl;
	}
	// Ratio de patrones de validacion
	if (cml["-v"]) {
		ratioValidacion = atof( cml("-v").c_str() );
		if (ratioValidacion < 0 || ratioValidacion > 1) {
			cerr << "Error: introduzca un porcentaje de patrones de validacion valido (parametro -v)." << endl;
			return EXIT_FAILURE;
		}
	} else {
		cout  << "Asumiendo valor por defecto (" << ratioValidacion << ") para el porcentaje de patrones de validacion (parametro -v)." << endl;
	}
	// Factor de decremento por capa
	if (cml["-d"]) {
		factorDecremento = atof( cml("-d").c_str() );
		if (factorDecremento <= 0) {
			cerr << "Error: introduzca un factor de decremento por capa valido (parametro -d)." << endl;
			return EXIT_FAILURE;
		}
	} else {
		cout << "Asumiendo valor por defecto (" << factorDecremento << ") para el factor de decremento por capa (parametro -d)." << endl;
	}
	// Version de entrenamiento (online/offline)
	if (cml["-o"]) {
		bOnline = true;
	} else {
		cout << "Asumiendo valor por defecto (" << bOnline << ") para el tipo de entrenamiento (parametro -o)." << endl;
	}
	// Tipo de funcion de error
	if (cml["-f"]) {
		tipoError = atoi( cml("-f").c_str() );
		if (tipoError < 0 || tipoError > 1) {
			cerr << "Error: introduzca un tipo de función de error valido (parametro -f)." << endl;
			return EXIT_FAILURE;
		}
	} else {
		cout << "Asumiendo valor por defecto (" << tipoError << ") para el tipo de función de error (parametro -f)." << endl;
	}
	// Uso de función softmax
	if (cml["-s"]) {
		usarSoftmax = true;
	} else {
		cout << "Asumiendo valor por defecto (" << usarSoftmax << ") para el uso de función softmax (parametro -s)." << endl;
	}

	// Objeto perceptrón multicapa
	PerceptronMulticapa mlp;

	// Parámetros del mlp
	mlp.dEta = eta;
	mlp.dMu = mu;
	mlp.dValidacion = ratioValidacion;
	mlp.dDecremento = factorDecremento;
	mlp.bOnline = bOnline;

	// Lectura de datos de entrenamiento y test: llamar a mlp.leerDatos(...)
	Datos* pDatosTrain = mlp.leerDatos(fichTrain.c_str());
	Datos* pDatosTest = mlp.leerDatos(fichTest.c_str());
	// Comprobar compatibilidad (mismo numero de entradas / salidas)
	if (pDatosTrain->nNumEntradas != pDatosTest->nNumEntradas
			|| pDatosTrain->nNumSalidas != pDatosTest->nNumSalidas) {
		cerr << "Error: los datos de entrenamiento y test no son compatibles." << endl;
		return EXIT_FAILURE;
	}

	// Inicializar vector topología
	int *topologia = new int[nCapas+2];
	topologia[0] = pDatosTrain->nNumEntradas;
	for(int i=1; i<(nCapas+1); i++) {
		topologia[i] = nNeuronas;
	}
	topologia[nCapas+1] = pDatosTrain->nNumSalidas;

	// Tipos de capa del MLP
	int *tipoCapas = new int[nCapas+2]();
	if (usarSoftmax) {
		// Aplicar softmax en la última capa
		tipoCapas[nCapas+1] = 1;
	}

	// Inicializar red con vector de topología
	mlp.inicializar(nCapas+2,topologia,tipoCapas);

	// Ajuste de eta y mu en funcion del tamaño del conjunto de entrenamiento (online)
	if (bOnline) {
		mlp.dEta /= (pDatosTrain->nNumPatrones * (1 - ratioValidacion));
		mlp.dMu /= (pDatosTrain->nNumPatrones * (1 - ratioValidacion));
	}


    // Semilla de los números aleatorios
    int semillas[] = {10,20,30,40,50};
    double *erroresTest = new double[5];
    double *erroresTrain = new double[5];
    double *ccrsTest = new double[5];
    double *ccrsTrain = new double[5];
    for(int i=0; i<5; i++){
    	cout << "**********" << endl;
    	cout << "SEMILLA " << semillas[i] << endl;
    	cout << "**********" << endl;
		srand(semillas[i]);
		mlp.ejecutarAlgoritmo(pDatosTrain,pDatosTest,nIteraciones,&(erroresTrain[i]),&(erroresTest[i]),&(ccrsTrain[i]),&(ccrsTest[i]),tipoError);
		cout << "Finalizamos => CCR de test final: "<< ccrsTest[i] << endl;
    }


    // Calcular medias y desviaciones típicas de entrenamiento y test
    double mediaErrorTest = 0, desviacionTipicaErrorTest = 0;
    double mediaErrorTrain = 0, desviacionTipicaErrorTrain = 0;
    double mediaCCRTest = 0, desviacionTipicaCCRTest = 0;
    double mediaCCRTrain = 0, desviacionTipicaCCRTrain = 0;

    // Media del error
	for (int i = 0; i < 5; i++) {
		mediaErrorTest += erroresTest[i];
		mediaErrorTrain += erroresTrain[i];
		mediaCCRTest += ccrsTest[i];
		mediaCCRTrain += ccrsTrain[i];
	}
	mediaErrorTest /= 5;
	mediaErrorTrain /= 5;
	mediaCCRTest /= 5;
	mediaCCRTrain /= 5;

	// Quasi-desviacion tipica del error (calculo de la raiz de la varianza)
	double mediaErrorTrainCuadrado = 0, mediaErrorTestCuadrado = 0;
	double mediaCCRTrainCuadrado = 0, mediaCCRTestCuadrado = 0;

	for (int i = 0; i < 5; i++) {
		mediaErrorTestCuadrado += pow(erroresTest[i] - mediaErrorTest, 2);
		mediaErrorTrainCuadrado += pow(erroresTrain[i] - mediaErrorTrain, 2);
		mediaCCRTrainCuadrado += pow(ccrsTrain[i] - mediaCCRTrain, 2);
		mediaCCRTestCuadrado += pow(ccrsTest[i] - mediaCCRTest, 2);
	}
	desviacionTipicaErrorTest = sqrt(mediaErrorTestCuadrado / 4);
	desviacionTipicaErrorTrain = sqrt(mediaErrorTrainCuadrado / 4);
	desviacionTipicaCCRTrain = sqrt(mediaCCRTrainCuadrado / 4);
	desviacionTipicaCCRTest = sqrt(mediaCCRTestCuadrado / 4);

	// Mostrar resultados
    cout << "HEMOS TERMINADO TODAS LAS SEMILLAS" << endl;
	cout << "INFORME FINAL" << endl;
	cout << "*************" << endl;
    cout << "Error de entrenamiento (Media +- DT): " << mediaErrorTrain << " +- " << desviacionTipicaErrorTrain << endl;
    cout << "Error de test (Media +- DT): " << mediaErrorTest << " +- " << desviacionTipicaErrorTest << endl;
    cout << "CCR de entrenamiento (Media +- DT): " << mediaCCRTrain << " +- " << desviacionTipicaCCRTrain << endl;
    cout << "CCR de test (Media +- DT): " << mediaCCRTest << " +- " << desviacionTipicaCCRTest << endl;

    // Liberacion de memoria
    delete[] tipoCapas;
    delete[] erroresTest;
    delete[] erroresTrain;
    delete[] ccrsTest;
    delete[] ccrsTrain;

    return EXIT_SUCCESS;
}

