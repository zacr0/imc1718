/*
 * util.h
 *
 *  Created on: 06/03/2015
 *      Author: pedroa
 */

#ifndef UTIL_H_
#define UTIL_H_

namespace util{
int * vectorAleatoriosEnterosSinRepeticion(int minimo, int maximo, int cuantos);

double realAleatorio(double min, double max);
}


#endif /* UTIL_H_ */
