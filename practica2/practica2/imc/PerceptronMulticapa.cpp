/*********************************************************************
 * File  : PerceptronMulticapa.cpp
 * Date  : 2017
 *********************************************************************/

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <cstdlib>  // Para establecer la semilla srand() y generar números aleatorios rand()
#include <limits>
#include <math.h>

#include "PerceptronMulticapa.h"
#include "util.h"

using namespace imc;
using namespace std;
using namespace util;


// ------------------------------
// CONSTRUCTOR: Dar valor por defecto a todos los parámetros (dEta, dMu, dValidacion y dDecremento)
PerceptronMulticapa::PerceptronMulticapa(){
	nNumCapas = 3;
	pCapas = NULL;
	dEta = 0.7;
	dMu = 1.0;
	dValidacion = 0;
	dDecremento = 1;
	bOnline = false;
}

// ------------------------------
// Reservar memoria para las estructuras de datos
// nl tiene el numero de capas y npl es un vector que contiene el número de neuronas por cada una de las capas
// tipo contiene el tipo de cada capa (0 => sigmoide, 1 => softmax)
// Rellenar vector Capa* pCapas
int PerceptronMulticapa::inicializar(int nl, int npl[], int tipo[]) {
	nNumCapas = nl;
	pCapas = new Capa[nl];

	// Reserva de capa de entrada
	pCapas[0].nNumNeuronas = npl[0];
	pCapas[0].pNeuronas = new Neurona[npl[0]];
	pCapas[0].tipo = tipo[0];

	// Reserva del resto de capas
	for (int i = 1; i < nl; i++) {
		pCapas[i].nNumNeuronas = npl[i];
		pCapas[i].pNeuronas = new Neurona[npl[i]];
		pCapas[i].tipo = tipo[i];

		// Reserva e inicializacion de vectores de cada neurona (mas sesgo)
		for (int j = 0; j < npl[i]; j++) {
			pCapas[i].pNeuronas[j].w = new double[ npl[i-1]+1 ]();
			pCapas[i].pNeuronas[j].deltaW = new double[ npl[i-1]+1 ]();
			pCapas[i].pNeuronas[j].ultimoDeltaW = new double[ npl[i-1]+1 ]();
			pCapas[i].pNeuronas[j].wCopia = new double[ npl[i-1]+1 ]();
		}
	}

	return 1;
}


// ------------------------------
// DESTRUCTOR: liberar memoria
PerceptronMulticapa::~PerceptronMulticapa() {
	liberarMemoria();
}


// ------------------------------
// Liberar memoria para las estructuras de datos
void PerceptronMulticapa::liberarMemoria() {
	for (int i = 0; i < nNumCapas; i++) {
		for (int j = 0; j < pCapas[i].nNumNeuronas; j++) {
			if (i != 0) { // la capa de entrada no tiene pesos
				delete[] pCapas[i].pNeuronas[j].w;
				delete[] pCapas[i].pNeuronas[j].deltaW;
				delete[] pCapas[i].pNeuronas[j].ultimoDeltaW;
				delete[] pCapas[i].pNeuronas[j].wCopia;
			}
		}
		delete[] pCapas[i].pNeuronas;
	}
	delete[] pCapas;
}

// ------------------------------
// Rellenar todos los pesos (w) aleatoriamente entre -1 y 1
void PerceptronMulticapa::pesosAleatorios() {
	int nNumNeuronasCapaAnteriorSesgo;

	for (int i = 1; i < nNumCapas; i++) {
		nNumNeuronasCapaAnteriorSesgo = pCapas[i-1].nNumNeuronas+1;

		for (int j = 0; j < pCapas[i].nNumNeuronas; j++) {
			/*
			 * Valor para cada peso de las neuronas de la capa actual con los las
			 * neuronas de la capa anterior, mas el sesgo.
			 */
			for (int k = 0; k < nNumNeuronasCapaAnteriorSesgo; k++) {
				pCapas[i].pNeuronas[j].w[k] = realAleatorio(-1, 1);
				// Copia de pesos
				pCapas[i].pNeuronas[j].wCopia[k] = pCapas[i].pNeuronas[j].w[k];
			}
		}
	}
}

// ------------------------------
// Alimentar las neuronas de entrada de la red con un patrón pasado como argumento
void PerceptronMulticapa::alimentarEntradas(double* input) {
	for (int i = 0; i < pCapas[0].nNumNeuronas; i++) {
		pCapas[0].pNeuronas[i].x = input[i];
		pCapas[0].pNeuronas[i].dX = input[i];
	}
}

// ------------------------------
// Recoger los valores predichos por la red (out de la capa de salida) y almacenarlos en el vector pasado como argumento
void PerceptronMulticapa::recogerSalidas(double* output){
	for (int i = 0; i < pCapas[nNumCapas-1].nNumNeuronas; i++) {
		output[i] = pCapas[nNumCapas-1].pNeuronas[i].x;
	}
}

// ------------------------------
// Hacer una copia de todos los pesos (copiar w en copiaW)
void PerceptronMulticapa::copiarPesos() {
	int nNumNeuronasCapaAnteriorSesgo;

	for (int i = 1; i < nNumCapas; i++) {
		nNumNeuronasCapaAnteriorSesgo = pCapas[i-1].nNumNeuronas+1;

		for (int j = 0; j < pCapas[i].nNumNeuronas; j++) {
			/*
			 * Valor para cada peso de las neuronas de la capa actual con los las
			 * neuronas de la capa anterior, mas el sesgo.
			 */
			for (int k = 0; k < nNumNeuronasCapaAnteriorSesgo; k++) {
				// Copia de pesos
				pCapas[i].pNeuronas[j].wCopia[k] = pCapas[i].pNeuronas[j].w[k];
			}
		}
	}
}

// ------------------------------
// Restaurar una copia de todos los pesos (copiar copiaW en w)
void PerceptronMulticapa::restaurarPesos() {
	int nNumNeuronasCapaAnteriorSesgo;

	for (int i = 1; i < nNumCapas; i++) {
		nNumNeuronasCapaAnteriorSesgo = pCapas[i-1].nNumNeuronas+1;

		for (int j = 0; j < pCapas[i].nNumNeuronas; j++) {
			/*
			 * Valor para cada peso de las neuronas de la capa actual con los las
			 * neuronas de la capa anterior, mas el sesgo.
			 */
			for (int k = 0; k < nNumNeuronasCapaAnteriorSesgo; k++) {
				pCapas[i].pNeuronas[j].w[k] = pCapas[i].pNeuronas[j].wCopia[k];
			}
		}
	}
}

// ------------------------------
// Calcular y propagar las salidas de las neuronas, desde la primera capa hasta la última
void PerceptronMulticapa::propagarEntradas() {
	double sumaAcumulada = 0; // net
	// Vector para almacenar la exponencial del net de cada neurona
	double net[pCapas[nNumCapas-1].nNumNeuronas];
	// Acumulacion de la exponencial de todas la neuronas
	double sumaNet = 0;

	// Propagacion a partir de la primera capa oculta
	for (int i = 1; i < nNumCapas; i++) {
		for (int j = 0; j < pCapas[i].nNumNeuronas; j++) {
			// Sumatorio de salidas en capa anterior por pesos de la neurona
			sumaAcumulada = 0;
			for (int k = 1; k < pCapas[i-1].nNumNeuronas+1; k++) {
				sumaAcumulada += pCapas[i-1].pNeuronas[k-1].x * pCapas[i].pNeuronas[j].w[k];
			}
			// Suma del sesgo (en la primera posicion)
			sumaAcumulada += pCapas[i].pNeuronas[j].w[0];

			// Tener en cuenta la función softmax
			if (pCapas[i].tipo == 1 && i == (nNumCapas-1)){
				// Acumulacion del net para aplicar la softmax
				net[j] = exp(sumaAcumulada);
				sumaNet += net[j];

			} else {
				// Calculo de funcion sigmoide
				pCapas[i].pNeuronas[j].x = 1/(1+exp(-1*sumaAcumulada));
			}

		}
	}

	// Calculo de la funcion softmax en la capa de salida, si esta activa
	if (pCapas[nNumCapas-1].tipo == 1) {
		for (int i = 0; i < pCapas[nNumCapas-1].nNumNeuronas; i++) {
			pCapas[nNumCapas-1].pNeuronas[i].x = net[i] / sumaNet;
		}
	}
}

// ------------------------------
// Calcular el error de salida del out de la capa de salida con respecto a un vector objetivo y devolverlo
// funcionError=1 => EntropiaCruzada // funcionError=0 => MSE
double PerceptronMulticapa::calcularErrorSalida(double* target, int funcionError) {
	double errorCapa = 0, // error final, mse o entropia cruzada
		   errorNeurona = 0;

	if (funcionError == 1) {
		// Calculo de la entropia cruzada
		for (int i = 0; i < pCapas[nNumCapas-1].nNumNeuronas; i++) {
			// Se calcula unicamente el bucle interno (sumatorio del error de las neuronas)
			if (pCapas[nNumCapas-1].pNeuronas[i].x > 0) {
				errorCapa += target[i] * log(pCapas[nNumCapas-1].pNeuronas[i].x);
				//cout << target[i] << " * " << log(pCapas[nNumCapas-1].pNeuronas[i].x) << " = " << (target[i] * log(pCapas[nNumCapas-1].pNeuronas[i].x)) << endl; //debug
			} else {
				// Evitar logaritmo de 0
				cout << "APLICANDO CORRECCION DEL LOGARITMO" << endl;//debug
				errorCapa += target[i] * log(0.000001);
			}
		}
		// Division entre el numero de salidas
		errorCapa /= pCapas[nNumCapas-1].nNumNeuronas;

	} else {
		// Calculo del MSE
		// Acumulacion del error respecto al valor deseado
		for (int i = 0; i < pCapas[nNumCapas-1].nNumNeuronas; i++) {
			errorNeurona = target[i] - pCapas[nNumCapas-1].pNeuronas[i].x;
			errorCapa += pow(errorNeurona, 2);
		}
		// Division entre el numero de salidas
		errorCapa /= pCapas[nNumCapas-1].nNumNeuronas;
	}

	return errorCapa;
}


// ------------------------------
// Retropropagar el error de salida con respecto a un vector pasado como argumento, desde la última capa hasta la primera
// funcionError=1 => EntropiaCruzada // funcionError=0 => MSE
void PerceptronMulticapa::retropropagarError(double* objetivo, int funcionError) {
	// indica si se deriva respecto a algo que esta detras de la neurona j
	int estaDetras = 0,
		nNumNeuronasUltimaCapa = pCapas[nNumCapas-1].nNumNeuronas;
	double salidaNeurona,
		   sumaAcumulada = 0,
		   aux = 0;

	if (pCapas[nNumCapas-1].tipo == 1) {
		// Derivadas para las neuronas de tipo softmax en la última capa
		for (int j = 0; j < nNumNeuronasUltimaCapa; j++) {
			sumaAcumulada = 0;

			for (int i = 0; i < nNumNeuronasUltimaCapa; i++) {
				salidaNeurona = pCapas[nNumCapas-1].pNeuronas[i].x;
				// Comprobar si se deriva respecto a una neurona que esta detras de la actual
				(j == i)? estaDetras = 1: estaDetras = 0;
				if (funcionError == 1) {
					// Entropia cruzada, dividir
					if (salidaNeurona != 0) {
						aux = objetivo[i] / salidaNeurona;
					} else {
						// Evitar division por cero
						cout << "EVITANDO DIVISION POR CERO" << endl;//debug
						aux = objetivo[i] / 0.00001;
					}

				} else {
					// Error MSE, restar
					aux = objetivo[i] - salidaNeurona;
				}
				sumaAcumulada += aux * pCapas[nNumCapas-1].pNeuronas[j].x * (estaDetras - salidaNeurona);
			}

			// Asignar derivada calculada a la neurona
			pCapas[nNumCapas-1].pNeuronas[j].dX = (-1) * sumaAcumulada;
		}

	} else {
		// Derivadas para las neuronas de tipo sigmoide en la capa de salida
		for (int i = 0; i < nNumNeuronasUltimaCapa; i++) {
			salidaNeurona = pCapas[nNumCapas-1].pNeuronas[i].x;
			if (funcionError == 1) {
				// Entropia cruzada, dividir
				if (salidaNeurona != 0) {
					aux = objetivo[i] / salidaNeurona;
				} else {
					// Evitar division por cero
					cout << "EVITANDO DIVISION POR CERO." << endl; //debug
					aux = objetivo[i] / 0.00001;
				}
			} else {
				// Error MSE, restar
				aux = objetivo[i] - salidaNeurona;
			}
			// Se ignora la constante 2
			pCapas[nNumCapas-1].pNeuronas[i].dX = (-1) * aux * salidaNeurona * (1-salidaNeurona);
		}
	}

	// Calculo de la derivada en el resto de capas (hacia atras)
	for (int i = nNumCapas-2; i >= 0; i--) {
		for (int j = 0; j < pCapas[i].nNumNeuronas; j++) {
			sumaAcumulada = 0;
			for (int k = 0; k < pCapas[i+1].nNumNeuronas; k++) {
				/*
				 * Producto de la salida por las derivadas de las neuronas de la
				 * capa siguiente conectadas con la neurona actual
				 */
				// j+1 ya que el sesgo se encuentra en la primera posicion
				sumaAcumulada += pCapas[i+1].pNeuronas[k].w[j+1] * pCapas[i+1].pNeuronas[k].dX;
			}
			// Calculo de la derivada
			salidaNeurona = pCapas[i].pNeuronas[j].x;
			pCapas[i].pNeuronas[j].dX = sumaAcumulada * salidaNeurona * (1-salidaNeurona);
		}
	}

}

// ------------------------------
// Acumular los cambios producidos por un patrón en deltaW
void PerceptronMulticapa::acumularCambio() {
	double derivadaSalidaNeurona,
		   salidaNeuronaCapaAnterior;

	for (int i = 1; i < nNumCapas; i++) {
		for (int j = 0; j < pCapas[i].nNumNeuronas; j++) {
			// Derivada de la salida de la neurona actual
			derivadaSalidaNeurona = pCapas[i].pNeuronas[j].dX;

			// Cambio aplicado a los pesos de las neuronas
			for (int k = 1; k < pCapas[i-1].nNumNeuronas+1; k++) { // sesgo en primera posicion
				salidaNeuronaCapaAnterior = pCapas[i-1].pNeuronas[k-1].x;
				pCapas[i].pNeuronas[j].deltaW[k] += derivadaSalidaNeurona * salidaNeuronaCapaAnterior;
			}
			// Cambio aplicado al sesgo
			pCapas[i].pNeuronas[j].deltaW[0] += derivadaSalidaNeurona;
		}
	}
}

// ------------------------------
// Actualizar los pesos de la red, desde la primera capa hasta la última
void PerceptronMulticapa::ajustarPesos() {
	double etaNeuronasCapa;
	// Numero de patrones entre el que se divide (offline -> nNumPatronesTrain)
	int n = 1;

	// Considerar entrenamiento offline
	bOnline? n = 1: n = nNumPatronesTrain;

	for (int i = 1; i < nNumCapas; i++) {
		// Decremento de la tasa de aprendizaje en funcion de la capa
		etaNeuronasCapa = pow(dDecremento, -1 * (nNumCapas-i)) * dEta;

		for (int j = 0; j < pCapas[i].nNumNeuronas; j++) {
			// Actualizacion de pesos entre neuronas de la capa anterior y sesgo
			for (int k = 0; k < pCapas[i-1].nNumNeuronas+1; k++) {
				pCapas[i].pNeuronas[j].w[k] += (-1) * ((etaNeuronasCapa * pCapas[i].pNeuronas[j].deltaW[k] + dMu * (etaNeuronasCapa * pCapas[i].pNeuronas[j].ultimoDeltaW[k])) / n);
			}
		}
	}
}

// ------------------------------
// Imprimir la red, es decir, todas las matrices de pesos
void PerceptronMulticapa::imprimirRed() {
	cout << "- Matriz de pesos (sesgo en primera posicion)" << endl;

	for (int i = 1; i < nNumCapas; i++) {
		cout << "- Capa " << i << endl;
		cout << "-------------------------" << endl;
		for (int j = 0; j < pCapas[i].nNumNeuronas; j++) {
			cout << "- Neurona " << j << ": ";
			for (int k = 0; k < pCapas[i-1].nNumNeuronas+1; k++) {
				cout << pCapas[i].pNeuronas[j].w[k] << " ";
			}
			cout << endl;
		}
		cout << endl;
	}
}

// ------------------------------
// Simular la red: propragar las entradas hacia delante, computar el error, retropropagar el error y ajustar los pesos
// entrada es el vector de entradas del patrón, objetivo es el vector de salidas deseadas del patrón.
// El paso de ajustar pesos solo deberá hacerse si el algoritmo es on-line
// Si no lo es, el ajuste de pesos hay que hacerlo en la función "entrenar"
// funcionError=1 => EntropiaCruzada // funcionError=0 => MSE
void PerceptronMulticapa::simularRed(double* entrada, double* objetivo, int funcionError) {
	if (bOnline) {
		// Reinicio de cambios de las neuronas (cambios por cada patron)
		for (int i = 1; i < nNumCapas; i++) {
			for (int j = 0; j < pCapas[i].nNumNeuronas; j++) {
				for (int k = 0; k < pCapas[i-1].nNumNeuronas+1; k++) {
					// Cambio aplicado pasa a ser el anterior
					pCapas[i].pNeuronas[j].ultimoDeltaW[k] = pCapas[i].pNeuronas[j].deltaW[k];
					pCapas[i].pNeuronas[j].deltaW[k] = 0;
				}
			}
		}
	}
	alimentarEntradas(entrada);
	propagarEntradas();
	retropropagarError(objetivo, funcionError);
	acumularCambio();
	if (bOnline) {
		// Ajustar pesos por cada patron
		ajustarPesos();
	}

}

// ------------------------------
// Leer una matriz de datos a partir de un nombre de fichero y devolverla
Datos* PerceptronMulticapa::leerDatos(const char *archivo) {
	fstream ficheroDatos;
	Datos* pDatos = new Datos;
	string buffer;

	// Apertura de fichero de datos
	ficheroDatos.open(archivo, fstream::in);
	if (ficheroDatos.is_open()) {
		// Lectura de fichero
		/*
		 * La estructura del fichero es:
		 * Primera linea: nº_entradas nº_salidas nº_patrones
		 * Resto de lineas: patrones
		 * Cada patron: nº_entradas entradas nº_salidas salidas
		 */
		// Primera linea
		ficheroDatos >> buffer;
		pDatos->nNumEntradas = atof(buffer.c_str());
		ficheroDatos >> buffer;
		pDatos->nNumSalidas = atof(buffer.c_str());
		ficheroDatos >> buffer;
		pDatos->nNumPatrones = atof(buffer.c_str());

		// Inicializacion de matrices
		pDatos->entradas = new double*[pDatos->nNumPatrones];
		for (int i = 0; i < pDatos->nNumPatrones; i++) {
			pDatos->entradas[i] = new double[pDatos->nNumEntradas];
		}
		pDatos->salidas = new double*[pDatos->nNumPatrones];
		for (int i = 0; i < pDatos->nNumPatrones; i++) {
			pDatos->salidas[i] = new double[pDatos->nNumSalidas];
		}

		// Lectura de patrones
		for (int i = 0; i < pDatos->nNumPatrones; i++) {
			// Lectura de entradas
			for (int j = 0; j < pDatos->nNumEntradas; j++) {
				ficheroDatos >> buffer;
				pDatos->entradas[i][j] = atof(buffer.c_str());
			}
			// Lectura de salidas
			for (int j = 0; j < pDatos->nNumSalidas; j++) {
				ficheroDatos >> buffer;
				pDatos->salidas[i][j] = atof(buffer.c_str());
			}
		}
		// Cierre de fichero
		ficheroDatos.close();
	} else {
		cerr << "Error: no ha podido abrirse el fichero \"" << archivo << "\"." << endl;
	}

	return pDatos;
}


// ------------------------------
// Entrenar la red para un determinado fichero de datos (pasar una vez por todos los patrones)
void PerceptronMulticapa::entrenar(Datos* pDatosTrain, int *idDatosValidation, int funcionError) {
	int idxValidacion = 0;

	for (int i = 1; i < nNumCapas; i++) {
		for (int j = 0; j < pCapas[i].nNumNeuronas; j++) {
			for (int k = 0; k < pCapas[i-1].nNumNeuronas+1; k++) {
				// Cambio aplicado pasa a ser el anterior
				pCapas[i].pNeuronas[j].ultimoDeltaW[k] = pCapas[i].pNeuronas[j].deltaW[k];
				pCapas[i].pNeuronas[j].deltaW[k] = 0;
			}
		}
	}

	// Entrenamiento
	if (idDatosValidation != NULL) {
		// Existen datos reservados para validacion
		for (int i = 0; i < pDatosTrain->nNumPatrones; i++) {
			// Se evitan los elementos seleccionados para validacion
			if (i != idDatosValidation[idxValidacion]) {
				// Patron de entrenamiento, entrenar con el
				simularRed(pDatosTrain->entradas[i], pDatosTrain->salidas[i], funcionError);
			} else {
				// Patron de validacion, ignorar
				idxValidacion++;
			}
		}
	} else {
		// No hay conjunto de validacion, se usan todos los patrones
		for (int i=0; i<pDatosTrain->nNumPatrones; i++){
			simularRed(pDatosTrain->entradas[i], pDatosTrain->salidas[i], funcionError);
		}
	}

	// Entrenamiento offline: Ajustar pesos tras entrenar con todos los patrones
	if (!bOnline) {
		ajustarPesos();
	}

}

// ------------------------------
// Probar la red con un conjunto de datos y devolver el error cometido
// funcionError=1 => EntropiaCruzada // funcionError=0 => MSE
double PerceptronMulticapa::test(Datos* pDatosTest, int *idDatosValidation, int nNumPatronesValidation, bool testValidacion, int funcionError) {
	double errorTest = 0;
	int idxValidacion = 0;

	if (idDatosValidation != NULL) {
		// Hay conjunto de  devalidacion
		if (testValidacion) {
			// Evaluacion sobre conjunto de validacion
			for (int i = 0; i < pDatosTest->nNumPatrones; i++) {
				if (i == idDatosValidation[idxValidacion]) {
					// Paso de los patrones por la red
					alimentarEntradas(pDatosTest->entradas[i]);
					propagarEntradas();
					// Acumulacion del MSE/entropia cruzada de cada patron
					errorTest += calcularErrorSalida(pDatosTest->salidas[i], funcionError);
					idxValidacion++;
				}

			}
			if (funcionError == 1) {
				// Calculo de la entropía cruzada de validación
				errorTest = -errorTest / nNumPatronesValidation;
			} else {
				// Calculo del MSE de validacion
				errorTest /= nNumPatronesValidation;
			}


		} else {
			// Evaluacion sobre conjunto de entrenamiento
			for (int i = 0; i < pDatosTest->nNumPatrones; i++) {
				if (i != idDatosValidation[idxValidacion]) {
					// Paso de los patrones por la red
					alimentarEntradas(pDatosTest->entradas[i]);
					propagarEntradas();
					// Acumulacion del MSE/entropia cruzada de cada patron
					errorTest += calcularErrorSalida(pDatosTest->salidas[i], funcionError);
				} else {
					// Patron de validacion, ignorar
					idxValidacion++;
				}
			}
			if (funcionError == 1) {
				// Calculo de la entropia cruzada de entrenamiento
				errorTest = -errorTest / nNumPatronesTrain;
			} else {
				// Calculo del MSE de entrenamiento
				errorTest /= nNumPatronesTrain;
			}

		}


	} else {
		// No hay conjunto de validacion
		// Evaluacion sobre conjunto completo
		for (int i = 0; i < pDatosTest->nNumPatrones; i++) {
			// Paso de los patrones por la red
			alimentarEntradas(pDatosTest->entradas[i]);
			propagarEntradas();
			// Acumulacion del MSE/entropia cruzada de cada patron
			errorTest += calcularErrorSalida(pDatosTest->salidas[i], funcionError);
		}
		if (funcionError == 1) {
			// Calculo de la entropia cruzada de test
			errorTest = -errorTest / pDatosTest->nNumPatrones;
		} else {
			// Calculo del MSE de test
			errorTest /= pDatosTest->nNumPatrones;
		}

	}

	return errorTest;
}


// ------------------------------
// Probar la red con un conjunto de datos y devolver el CCR
double PerceptronMulticapa::testClassification(Datos* pDatosTest, int *idDatosValidation) {
	double ccr = 0;
	double maxSalidaPredicha = -1,
		   maxSalidaReal = -1;
	int idMaxSalidaPredicha = -1,
		idMaxSalidaReal = -1;
	// Matriz de confusion
	//int matrizConfusion[pDatosTest->nNumSalidas][pDatosTest->nNumSalidas];

	// Inicializacion de matriz de confusion
	/*for (int i = 0; i < pDatosTest->nNumSalidas; i++) {
		for (int j = 0; j < pDatosTest->nNumSalidas; j++) {
			matrizConfusion[i][j] = 0;
		}
	}*/

	if (idDatosValidation != NULL) {
		// Hay conjunto de validacion, se deben evitar dichos patrones
		int idxValidacion = 0;

		for (int i = 0; i < pDatosTest->nNumPatrones; i++) {
			if (i != idDatosValidation[idxValidacion]) {
				// Patron de entrenamiento, tenerlo en cuenta para el calculo
				// Paso de los patrones por la red
				alimentarEntradas(pDatosTest->entradas[i]);
				propagarEntradas();

				// Busqueda de clase con valor de salida mas alto
				maxSalidaPredicha = -1;
				maxSalidaReal = -1;
				idMaxSalidaPredicha = -1;
				idMaxSalidaReal = -1;

				for (int j = 0; j < pCapas[nNumCapas-1].nNumNeuronas; j++) {
					if (pDatosTest->salidas[i][j] > maxSalidaReal) {
						maxSalidaReal = pDatosTest->salidas[i][j];
						idMaxSalidaReal = j;
					}
					if (pCapas[nNumCapas-1].pNeuronas[j].x > maxSalidaPredicha) {
						maxSalidaPredicha = pCapas[nNumCapas-1].pNeuronas[j].x;
						idMaxSalidaPredicha = j;
					}

				}
				if (idMaxSalidaPredicha == idMaxSalidaReal) {
					// Acierto
					ccr++;
				}
				// Actualizar matriz de confusion
				//matrizConfusion[idMaxSalidaPredicha][idMaxSalidaReal]++;

			} else {
				// Patron de validacion, ignorar
				idxValidacion++;
			}
		}
		// Conversion del CCR a porcentaje
		ccr = 100 * (ccr / nNumPatronesTrain);

	} else {
		// No hay conjunto de validacion, evaluar sobre todos los patrones
		for (int i = 0; i < pDatosTest->nNumPatrones; i++) {
			// Paso de los patrones por la red
			alimentarEntradas(pDatosTest->entradas[i]);
			propagarEntradas();

			// Busqueda de clase con valor de salida mas alto
			maxSalidaPredicha = -1;
			maxSalidaReal = -1;
			idMaxSalidaPredicha = -1;
			idMaxSalidaReal = -1;

			for (int j = 0; j < pCapas[nNumCapas-1].nNumNeuronas; j++) {
				if (pDatosTest->salidas[i][j] > maxSalidaReal) {
					maxSalidaReal = pDatosTest->salidas[i][j];
					idMaxSalidaReal = j;
				}
				 if (pCapas[nNumCapas-1].pNeuronas[j].x > maxSalidaPredicha) {
					maxSalidaPredicha = pCapas[nNumCapas-1].pNeuronas[j].x;
					idMaxSalidaPredicha = j;
				}

			}
			if (idMaxSalidaPredicha == idMaxSalidaReal) {
				// Acierto
				ccr++;
			}// else {
				//cout << "Patron " << i << ": confunde clase (real) " << idMaxSalidaReal << " con clase (predicha) " << idMaxSalidaPredicha << endl;
			//}
			// Actualizar matriz de confusion
			//matrizConfusion[idMaxSalidaPredicha][idMaxSalidaReal]++;

		}
		// Conversion del CCR a porcentaje
		ccr = 100 * (ccr / pDatosTest->nNumPatrones);
	}

	// Mostrar matriz de confusión
	/*cout << "Clase predicha/Clase real" << endl;
	for (int i = 0; i < pDatosTest->nNumSalidas; i++) {
		for (int j = 0; j < pDatosTest->nNumSalidas; j++) {
			cout << matrizConfusion[i][j] << "\t";
		}
		cout << endl;
	}*/

	return ccr;
}

// ------------------------------
// Ejecutar el algoritmo de entrenamiento durante un número de iteraciones, utilizando pDatosTrain
// Una vez terminado, probar como funciona la red en pDatosTest
// Tanto el error MSE de entrenamiento como el error MSE de test debe calcularse y almacenarse en errorTrain y errorTest
// funcionError=1 => EntropiaCruzada // funcionError=0 => MSE
void PerceptronMulticapa::ejecutarAlgoritmo(Datos * pDatosTrain, Datos * pDatosTest, int maxiter, double *errorTrain, double *errorTest, double *ccrTrain, double *ccrTest, int funcionError)
{
	int countTrain = 0;

	// Inicialización de pesos
	pesosAleatorios();

	int numSinMejorarTrain = 0,
		numSinMejorarValidacion = 0;
	double trainError = 0,
		   testError = 0,
		   validationError = 0,
		   minTrainError = 0,
		   minValidationError = 0,
		   tolerancia = 0.00001;
	nNumPatronesTrain = pDatosTrain->nNumPatrones;

	// Fichero de datos de salida (graficas de convergencia)
	//fstream fichSalida;
	//string archivoSalida = "datos_convergencia.txt";

	// Generar datos de validación
	int *idDatosValidation = NULL,
		nNumPatronesValidation = 0;
	bool validacion = false;
	if(dValidacion > 0 && dValidacion < 1){
		// Cantidad de patrones de validacion
		nNumPatronesValidation = round(dValidacion * pDatosTrain->nNumPatrones);
		// Descontar los patrones de validacion del conjunto de entrenamiento
		nNumPatronesTrain -= nNumPatronesValidation;
		// Seleccion de patrones del conjunto de entrenamiento
		idDatosValidation = vectorAleatoriosEnterosSinRepeticion(0, pDatosTrain->nNumPatrones, nNumPatronesValidation);
		validacion = true;
	}

	// Formato de cabecera de fichero de salida
	//fichSalida.open(archivoSalida.c_str(), fstream::out);
	//fichSalida << "Iteracion CCR-train CCR-test" << endl;

	// Aprendizaje del algoritmo (bucle externo)
	do {
		// Entrenamiento
		entrenar(pDatosTrain, idDatosValidation, funcionError);

		// Evaluacion en el conjunto de entrenamiento
		trainError = test(pDatosTrain, idDatosValidation, nNumPatronesValidation, false, funcionError);
		if(countTrain==0 || fabs(trainError - minTrainError) > tolerancia){
			// Error de entrenamiento mejora por encima del minimo
			//if (countTrain == 0 || trainError < minTrainError) {
				// Si el error mejora se copian los pesos
				minTrainError = trainError;
				copiarPesos();
				numSinMejorarTrain = 0;
			//}
		} else{
			numSinMejorarTrain++;
		}

		// Detener el entrenamiento si no mejora
		if(numSinMejorarTrain==50) {
			countTrain = maxiter;
		}

		// Error de test
		testError = test(pDatosTest, NULL, 0, false, funcionError);

		// Error de validacion
		if (validacion) {
			// Comprobar condiciones de parada de validación y forzar
			validationError = test(pDatosTrain, idDatosValidation, nNumPatronesValidation, true, funcionError);
			if(countTrain==0 || fabs(validationError - minValidationError) > tolerancia){
				// Error de validacion mejora por encima del minimo
				minValidationError = validationError;
				numSinMejorarValidacion = 0;
			} else{
				numSinMejorarValidacion++;
			}

			// Detener el entrenamiento si no mejora validacion
			if (numSinMejorarValidacion==50){
				countTrain = maxiter;
			}
		}

		// Volcado de CCR en fichero de salida
		//fichSalida << countTrain << " " <<  testClassification(pDatosTrain, idDatosValidation) << " " << testClassification(pDatosTest, NULL) << endl;

		countTrain++;
		cout << "Iteración " << countTrain << "\t Error de entrenamiento: " << trainError << "\t Error de test: " << testError << "\t Error de validacion: " << validationError << endl;
	} while ( countTrain<maxiter );

	// Cierre de fichero
	//fichSalida.close();

	// Se restauran los mejores pesos
	restaurarPesos();

	cout << "PESOS DE LA RED" << endl;
	cout << "===============" << endl;
	imprimirRed();
	

	cout << "Salida Esperada Vs Salida Obtenida (test)" << endl;
	cout << "=========================================" << endl;
	for(int i=0; i<pDatosTest->nNumPatrones; i++){
		double* prediccion = new double[pDatosTest->nNumSalidas];

		// Cargamos las entradas y propagamos el valor
		alimentarEntradas(pDatosTest->entradas[i]);
		propagarEntradas();
		recogerSalidas(prediccion);
		for(int j=0; j<pDatosTest->nNumSalidas; j++)
			cout << pDatosTest->salidas[i][j] << " -- " << prediccion[j] << " \\\\ " ;
		cout << endl;
		delete[] prediccion;

	}

	*errorTest = test(pDatosTest, NULL, 0, false, funcionError);
	*errorTrain = minTrainError;
	*ccrTest = testClassification(pDatosTest, NULL);
	*ccrTrain = testClassification(pDatosTrain, idDatosValidation);

}

