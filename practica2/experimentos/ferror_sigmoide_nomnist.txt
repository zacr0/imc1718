#Fichero de arquitecturas 
#H: marca las neuronas por capa
#L: marca el numero de capas.
#E: factor de aprendizaje
#M: momento
#V: validacion
#D: decremento
#F: funcion de error (1: entropia cruzada, 0: mse)
#I: numero de iteraciones
#O: online (1: si/online, 0: no/offline)
#S: softmax (1: softmax capa salida, 0: sigmoide)

#Se hacen todas las combinaciones posibles entre los parametros

#Neuronas
H:75

#Capas
L:2

#Factor aprendizaje
E:0.7

#Momento
M:1.0

#Validacion
V:0.1

#Decremento
D:1.0

#FUNCION ERROR
F:0

#NUMERO DE ITERACIONES
I:500

#ONLINE
O:0

#SOFTMAX
S:0
