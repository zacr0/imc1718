/*********************************************************************
 * File  : PerceptronMulticapa.cpp
 * Date  : 2017
 *********************************************************************/

#include "PerceptronMulticapa.h"
#include "util.h"


#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <cstdlib>  // Para establecer la semilla srand() y generar números aleatorios rand()
#include <limits>
#include <math.h>


using namespace imc;
using namespace std;
using namespace util;

// ------------------------------
// CONSTRUCTOR: Dar valor por defecto a todos los parámetros
PerceptronMulticapa::PerceptronMulticapa(){
	nNumCapas = 3;
	pCapas = NULL;
	dEta = 0.1;
	dMu = 0.9;
	dValidacion = 0;
	dDecremento = 1;
}

// ------------------------------
// Reservar memoria para las estructuras de datos
int PerceptronMulticapa::inicializar(int nl, int npl[]) {
	nNumCapas = nl;
	pCapas = new Capa[nl];

	// Reserva de capa de entrada
	pCapas[0].nNumNeuronas = npl[0];
	pCapas[0].pNeuronas = new Neurona[npl[0]];

	// Reserva del resto de capas
	for (int i = 1; i < nl; i++) {
		pCapas[i].nNumNeuronas = npl[i];
		pCapas[i].pNeuronas = new Neurona[npl[i]];

		// Reserva e inicializacion de vectores de cada neurona
		for (int j = 0; j < npl[i]; j++) {
			pCapas[i].pNeuronas[j].w = new double[ npl[i-1]+1 ]();
			pCapas[i].pNeuronas[j].deltaW = new double[ npl[i-1]+1 ]();
			pCapas[i].pNeuronas[j].ultimoDeltaW = new double[ npl[i-1]+1 ]();
			pCapas[i].pNeuronas[j].wCopia = new double[ npl[i-1]+1 ]();
		}
	}

	return 1;
}


// ------------------------------
// DESTRUCTOR: liberar memoria
PerceptronMulticapa::~PerceptronMulticapa() {
	liberarMemoria();
}


// ------------------------------
// Liberar memoria para las estructuras de datos
void PerceptronMulticapa::liberarMemoria() {

	for (int i = 0; i < nNumCapas; i++) {
		for (int j = 0; j < pCapas[i].nNumNeuronas; j++) {
			if (i != 0) { // la capa de entrada no tiene pesos
				delete[] pCapas[i].pNeuronas[j].w;
				delete[] pCapas[i].pNeuronas[j].deltaW;
				delete[] pCapas[i].pNeuronas[j].ultimoDeltaW;
				delete[] pCapas[i].pNeuronas[j].wCopia;
			}
		}
		delete[] pCapas[i].pNeuronas;
	}
	delete[] pCapas;

}

// ------------------------------
// Rellenar todos los pesos (w) aleatoriamente entre -1 y 1
void PerceptronMulticapa::pesosAleatorios() {
	int nNumNeuronasCapaAnteriorSesgo;

	for (int i = 1; i < nNumCapas; i++) {
		nNumNeuronasCapaAnteriorSesgo = pCapas[i-1].nNumNeuronas+1;

		for (int j = 0; j < pCapas[i].nNumNeuronas; j++) {
			/*
			 * Valor para cada peso de las neuronas de la capa actual con los las
			 * neuronas de la capa anterior, mas el sesgo.
			 */
			for (int k = 0; k < nNumNeuronasCapaAnteriorSesgo; k++) {
				pCapas[i].pNeuronas[j].w[k] = realAleatorio(-1, 1);
				// Copia de pesos
				pCapas[i].pNeuronas[j].wCopia[k] = pCapas[i].pNeuronas[j].w[k];
			}
		}
	}
}

// ------------------------------
// Alimentar las neuronas de entrada de la red con un patrón pasado como argumento
void PerceptronMulticapa::alimentarEntradas(double* input) {
	for (int i = 0; i < pCapas[0].nNumNeuronas; i++) {
		pCapas[0].pNeuronas[i].x = input[i];
		pCapas[0].pNeuronas[i].dX = input[i];
	}
}

// ------------------------------
// Recoger los valores predichos por la red (out de la capa de salida) y almacenarlos en el vector pasado como argumento
void PerceptronMulticapa::recogerSalidas(double* output)
{
	for (int i = 0; i < pCapas[nNumCapas-1].nNumNeuronas; i++) {
		output[i] = pCapas[nNumCapas-1].pNeuronas[i].x;
	}
}

// ------------------------------
// Hacer una copia de todos los pesos (copiar w en copiaW)
void PerceptronMulticapa::copiarPesos() {
 	int nNumNeuronasCapaAnteriorSesgo;

	for (int i = 1; i < nNumCapas; i++) {
		nNumNeuronasCapaAnteriorSesgo = pCapas[i-1].nNumNeuronas+1;

		for (int j = 0; j < pCapas[i].nNumNeuronas; j++) {
			/*
			 * Valor para cada peso de las neuronas de la capa actual con los las
			 * neuronas de la capa anterior, mas el sesgo.
			 */
			for (int k = 0; k < nNumNeuronasCapaAnteriorSesgo; k++) {
				// Copia de pesos
				pCapas[i].pNeuronas[j].wCopia[k] = pCapas[i].pNeuronas[j].w[k];
			}
		}
	}
}

// ------------------------------
// Restaurar una copia de todos los pesos (copiar copiaW en w)
void PerceptronMulticapa::restaurarPesos() {
	int nNumNeuronasCapaAnteriorSesgo;

	for (int i = 1; i < nNumCapas; i++) {
		nNumNeuronasCapaAnteriorSesgo = pCapas[i-1].nNumNeuronas+1;

		for (int j = 0; j < pCapas[i].nNumNeuronas; j++) {
			/*
			 * Valor para cada peso de las neuronas de la capa actual con los las
			 * neuronas de la capa anterior, mas el sesgo.
			 */
			for (int k = 0; k < nNumNeuronasCapaAnteriorSesgo; k++) {
				pCapas[i].pNeuronas[j].w[k] = pCapas[i].pNeuronas[j].wCopia[k];
			}
		}
	}
}

// ------------------------------
// Calcular y propagar las salidas de las neuronas, desde la primera capa hasta la última
void PerceptronMulticapa::propagarEntradas() {
	double sumaAcumulada = 0;

	// Propagacion a partir de la primera capa oculta
	for (int i = 1; i < nNumCapas; i++) {
		for (int j = 0; j < pCapas[i].nNumNeuronas; j++) {
			// Sumatorio de salidas en capa anterior por pesos de la neurona
			sumaAcumulada = 0;
			for (int k = 1; k < pCapas[i-1].nNumNeuronas+1; k++) {
				sumaAcumulada += pCapas[i-1].pNeuronas[k-1].x * pCapas[i].pNeuronas[j].w[k];
			}
			// Suma del sesgo (en la primera posicion)
			sumaAcumulada += pCapas[i].pNeuronas[j].w[0];
			// Calculo de funcion sigmoide
			pCapas[i].pNeuronas[j].x = 1/(1+exp(-1*sumaAcumulada));
		}
	}

}

// ------------------------------
// Calcular el error de salida (MSE) del out de la capa de salida con respecto a un vector objetivo y devolverlo
double PerceptronMulticapa::calcularErrorSalida(double* target) {
	double mse = 0,
		   error = 0;

	// Acumulacion del error respecto al valor deseado
	for (int i = 0; i < pCapas[nNumCapas-1].nNumNeuronas; i++) {
		error = target[i] - pCapas[nNumCapas-1].pNeuronas[i].x;
		mse += pow(error, 2);
	}
	// Division entre el numero de salidas
	mse /= pCapas[nNumCapas-1].nNumNeuronas;

	return mse;
}


// ------------------------------
// Retropropagar el error de salida con respecto a un vector pasado como argumento, desde la última capa hasta la primera
void PerceptronMulticapa::retropropagarError(double* objetivo) {
	double salidaNeurona,
		   sumaAcumulada = 0;

	// Calculo de la derivada en la capa de salida
	for (int i = 0; i < pCapas[nNumCapas-1].nNumNeuronas; i++) {
		salidaNeurona = pCapas[nNumCapas-1].pNeuronas[i].x;
		// Se ignora la constante 2
		pCapas[nNumCapas-1].pNeuronas[i].dX = -1*(objetivo[i]-salidaNeurona) * salidaNeurona*(1-salidaNeurona);
	}

	// Calculo de la derivada en el resto de capas (hacia atras)
	for (int i = nNumCapas-2; i >= 0; i--) {
		for (int j = 0; j < pCapas[i].nNumNeuronas; j++) {
			sumaAcumulada = 0;
			for (int k = 0; k < pCapas[i+1].nNumNeuronas; k++) {
				/*
				 * Producto de la salida por las derivadas de las neuronas de la
				 * capa siguiente conectadas con la neurona actual
				 */
				// j+1 ya que el sesgo se encuentra en la primera posicion
				sumaAcumulada += pCapas[i+1].pNeuronas[k].w[j+1] * pCapas[i+1].pNeuronas[k].dX;
				//sumaAcumulada += pCapas[i+1].pNeuronas[k].w[j+1] * pCapas[i+1].pNeuronas[k-1].dX;
			}
			// Calculo de la derivada
			salidaNeurona = pCapas[i].pNeuronas[j].x;
			pCapas[i].pNeuronas[j].dX = sumaAcumulada * salidaNeurona * (1-salidaNeurona);
		}
	}


}

// ------------------------------
// Acumular los cambios producidos por un patrón en deltaW
void PerceptronMulticapa::acumularCambio() {
	double derivadaSalidaNeurona,
		   salidaNeuronaCapaAnterior;

	for (int i = 1; i < nNumCapas; i++) {
		for (int j = 0; j < pCapas[i].nNumNeuronas; j++) {
			// Derivada de la salida de la neurona actual
			derivadaSalidaNeurona = pCapas[i].pNeuronas[j].dX;

			// Cambio aplicado a los pesos de las neuronas
			for (int k = 1; k < pCapas[i-1].nNumNeuronas+1; k++) { // sesgo en primera posicion
				salidaNeuronaCapaAnterior = pCapas[i-1].pNeuronas[k-1].x;
				//salidaNeuronaCapaAnterior = pCapas[i-1].pNeuronas[k].x;
				pCapas[i].pNeuronas[j].deltaW[k] += derivadaSalidaNeurona * salidaNeuronaCapaAnterior;
			}
			// Cambio aplicado al sesgo
			pCapas[i].pNeuronas[j].deltaW[0] += derivadaSalidaNeurona;
		}
	}
}

// ------------------------------
// Actualizar los pesos de la red, desde la primera capa hasta la última
void PerceptronMulticapa::ajustarPesos() {
	double etaNeuronasCapa;

	for (int i = 1; i < nNumCapas; i++) {
		for (int j = 0; j < pCapas[i].nNumNeuronas; j++) {
			// Decremento de la tasa de aprendizaje en funcion de la capa
			etaNeuronasCapa = pow(dDecremento, -1 * (nNumCapas-i)) * dEta;

			// Actualizacion de pesos entre neuronas de la capa anterior y sesgo
			for (int k = 0; k < pCapas[i-1].nNumNeuronas+1; k++) {
				pCapas[i].pNeuronas[j].w[k] += -1 * etaNeuronasCapa * pCapas[i].pNeuronas[j].deltaW[k] - dMu * (etaNeuronasCapa * pCapas[i].pNeuronas[j].ultimoDeltaW[k]);
			}
		}
	}
}

// ------------------------------
// Imprimir la red, es decir, todas las matrices de pesos
void PerceptronMulticapa::imprimirRed() {
	cout << "- Matriz de pesos (sesgo en primera posicion)" << endl;

	for (int i = 1; i < nNumCapas; i++) {
		cout << "- Capa " << i << endl;
		for (int j = 0; j < pCapas[i].nNumNeuronas; j++) {
			cout << "- Neurona " << j << ": ";
			for (int k = 0; k < pCapas[i-1].nNumNeuronas+1; k++) {
				cout << pCapas[i].pNeuronas[j].w[k] << " ";
			}
			cout << endl;
		}
	}
}

// ------------------------------
// Simular la red: propagar las entradas hacia delante, retropropagar el error y ajustar los pesos
// entrada es el vector de entradas del patrón y objetivo es el vector de salidas deseadas del patrón
void PerceptronMulticapa::simularRedOnline(double* entrada, double* objetivo) {
	// Reinicio de cambios de las neuronas (cambios por cada patron)
	for (int i = 1; i < nNumCapas; i++) {
		for (int j = 0; j < pCapas[i].nNumNeuronas; j++) {
			//for (int k = 0; k < pCapas[i-1].nNumNeuronas; k++) {
			for (int k = 0; k < pCapas[i-1].nNumNeuronas+1; k++) {
				// Cambio aplicado pasa a ser el anterior
				pCapas[i].pNeuronas[j].ultimoDeltaW[k] = pCapas[i].pNeuronas[j].deltaW[k];
				pCapas[i].pNeuronas[j].deltaW[k] = 0;
			}
		}
	}

	// Proceso de entrenamiento
	alimentarEntradas(entrada);
	propagarEntradas();
	retropropagarError(objetivo);
	acumularCambio();
	ajustarPesos();
}

// ------------------------------
// Leer una matriz de datos a partir de un nombre de fichero y devolverla
Datos* PerceptronMulticapa::leerDatos(const char *archivo) {
	fstream ficheroDatos;
	Datos* datos = new Datos;
	string buffer;

	// Apertura de fichero de datos
	ficheroDatos.open(archivo, fstream::in);
	if (ficheroDatos.is_open()) {
		// Lectura de fichero
		/*
		 * La estructura del fichero es:
		 * Primera linea: nº_entradas nº_salidas nº_patrones
		 * Resto de lineas: patrones
		 * Cada patron: nº_entradas entradas nº_salidas salidas
		 */
		// Primera linea
		ficheroDatos >> buffer;
		datos->nNumEntradas = atof(buffer.c_str());
		ficheroDatos >> buffer;
		datos->nNumSalidas = atof(buffer.c_str());
		ficheroDatos >> buffer;
		datos->nNumPatrones = atof(buffer.c_str());

		// Inicializacion de matrices
		datos->entradas = new double*[datos->nNumPatrones];
		for (int i = 0; i < datos->nNumPatrones; i++) {
			datos->entradas[i] = new double[datos->nNumEntradas];
		}
		datos->salidas = new double*[datos->nNumPatrones];
		for (int i = 0; i < datos->nNumPatrones; i++) {
			datos->salidas[i] = new double[datos->nNumSalidas];
		}

		// Lectura de patrones
		for (int i = 0; i < datos->nNumPatrones; i++) {
			for (int j = 0; j < datos->nNumEntradas; j++) {
				ficheroDatos >> buffer;
				datos->entradas[i][j] = atof(buffer.c_str());
			}
			for (int j = 0; j < datos->nNumSalidas; j++) {
				ficheroDatos >> buffer;
				datos->salidas[i][j] = atof(buffer.c_str());
			}
		}
		// Cierre de fichero
		ficheroDatos.close();
	} else {
		cerr << "Error: no ha podido abrirse el fichero \"" << archivo << "\"." << endl;
	}

	return datos;
}

// ------------------------------
// Entrenar la red on-line para un determinado fichero de datos
void PerceptronMulticapa::entrenarOnline(Datos* pDatosTrain, int *idDatosValidation) {
	int idxValidacion = 0;

	if (idDatosValidation != NULL) {
		// Existen datos reservados para validacion
		for (int i = 0; i < pDatosTrain->nNumPatrones; i++) {
			// Se evitan los elementos seleccionados para validacion
			if (i != idDatosValidation[idxValidacion]) {
				// Patron de entrenamiento, utilizar
				simularRedOnline(pDatosTrain->entradas[i], pDatosTrain->salidas[i]);
			} else {
				// Patron de validacion, ignorar
				idxValidacion++;
			}
		}
	} else {
		// No hay conjunto de validacion, se usan todos los patrones
		for (int i=0; i<pDatosTrain->nNumPatrones; i++){
			simularRedOnline(pDatosTrain->entradas[i], pDatosTrain->salidas[i]);
		}
	}

}

// ------------------------------
// Probar la red con un conjunto de datos y devolver el error MSE cometido
double PerceptronMulticapa::test(Datos* pDatosTest, int *idDatosValidation, int nNumPatronesValidation, bool testValidacion) {
	double mse = 0;
	int idxValidacion = 0;

	if (idDatosValidation != NULL) {
		// Hay conjunto de validacion
		if (testValidacion) {
			// Evaluacion sobre conjunto de validacion
			for (int i = 0; i < pDatosTest->nNumPatrones; i++) {
				if (i == idDatosValidation[idxValidacion]) {
					// Paso de los patrones por la red
					alimentarEntradas(pDatosTest->entradas[i]);
					propagarEntradas();
					// Acumulacion del MSE de cada patron
					mse += calcularErrorSalida(pDatosTest->salidas[i]);
					idxValidacion++;
				}

			}
			// Calculo del MSE de validacion
			mse /= nNumPatronesValidation;

		} else {
			// Entrenamiento sobre conjunto de entrenamiento
			for (int i = 0; i < pDatosTest->nNumPatrones; i++) {
				if (i != idDatosValidation[idxValidacion]) {
					// Paso de los patrones por la red
					alimentarEntradas(pDatosTest->entradas[i]);
					propagarEntradas();
					// Acumulacion del MSE de cada patron
					mse += calcularErrorSalida(pDatosTest->salidas[i]);
				} else {
					idxValidacion++;
				}
			}
			// Calculo del MSE de entrenamiento
			mse /= (pDatosTest->nNumPatrones - nNumPatronesValidation);
		}


	} else {
		// Evaluacion sobre conjunto completo
		for (int i = 0; i < pDatosTest->nNumPatrones; i++) {
			// Paso de los patrones por la red
			alimentarEntradas(pDatosTest->entradas[i]);
			propagarEntradas();
			// Acumulacion del MSE de cada patron
			mse += calcularErrorSalida(pDatosTest->salidas[i]);
		}
		// Calculo del MSE de test
		mse /= pDatosTest->nNumPatrones;
	}

	return mse;
}

// ------------------------------
// Ejecutar el algoritmo de entrenamiento durante un número de iteraciones, utilizando pDatosTrain
// Una vez terminado, probar como funciona la red en pDatosTest
// Tanto el error MSE de entrenamiento como el error MSE de test debe calcularse y almacenarse en trainError y testError
void PerceptronMulticapa::ejecutarAlgoritmoOnline(Datos * pDatosTrain, Datos * pDatosTest, int maxiter, double *errorTrain, double *errorTest)
{
	int countTrain = 0;

	// Inicialización de pesos
	pesosAleatorios();

	int numSinMejorarTrain = 0,
		numSinMejorarValidation = 0;
	double trainError = 0,
		   minTrainError = 0,
		   testError = 0,
		   validationError = 0,
		   minValidationError = 0,
		   tolerancia = 0.00001;

	// Seleccionar patrones de validación
	int *idDatosValidation = NULL,
		nNumPatronesValidation = 0;
	bool validacion = false;
	if(dValidacion > 0 && dValidacion < 1){
		// Cantidad de patrones de validacion
		nNumPatronesValidation = round(dValidacion*pDatosTrain->nNumPatrones);
		// Seleccion de patrones del conjunto de entrenamiento
		idDatosValidation = vectorAleatoriosEnterosSinRepeticion(0, pDatosTrain->nNumPatrones, nNumPatronesValidation);
		validacion = true;
	}


	// Aprendizaje del algoritmo (bucle externo)
	do {
		// Entrenamiento
		entrenarOnline(pDatosTrain, idDatosValidation);
		// Evaluacion en el conjunto de entrenamiento
		trainError = test(pDatosTrain, idDatosValidation, nNumPatronesValidation, false);
		if(countTrain==0 || fabs(trainError - minTrainError) > tolerancia){
			// Error de entrenamiento mejora por encima del minimo
			minTrainError = trainError;
			copiarPesos();
			numSinMejorarTrain = 0;
		} else{
			numSinMejorarTrain++;
		}

		// Detener el entrenamiento si no mejora entrenamiento
		if(numSinMejorarTrain==50){
			// Condicion de parada, se restauran los mejores resultados
			restaurarPesos();
			// Forzar salida
			countTrain = maxiter; 
		}

		if (validacion) {
			// Comprobar condiciones de parada de validación y forzar
			validationError = test(pDatosTrain, idDatosValidation, nNumPatronesValidation, true);
			if(countTrain==0 || fabs(validationError - minValidationError) > tolerancia){
				// Error de validacion mejora por encima del minimo
				minValidationError = validationError;
				numSinMejorarValidation = 0;
			} else{
				numSinMejorarValidation++;
			}

			// Detener el entrenamiento si no mejora validacion
			if(numSinMejorarValidation==50){
				// Condicion de parada, se restauran los mejores resultados
				restaurarPesos();
				// Forzar salida
				countTrain = maxiter;
			}
		}

		countTrain++;
		cout << "Iteración " << countTrain << "\t Error de entrenamiento: " << trainError << "\t Error de validación: " << validationError << endl;

	} while ( countTrain<maxiter );

	cout << "PESOS DE LA RED" << endl;
	cout << "===============" << endl;
	imprimirRed();

	cout << "Salida Esperada Vs Salida Obtenida (test)" << endl;
	cout << "=========================================" << endl;
	for(int i=0; i<pDatosTest->nNumPatrones; i++){
		double* prediccion = new double[pDatosTest->nNumSalidas];

		// Cargamos las entradas y propagamos el valor
		alimentarEntradas(pDatosTest->entradas[i]);
		propagarEntradas();
		recogerSalidas(prediccion);
		for(int j=0; j<pDatosTest->nNumSalidas; j++)
			cout << pDatosTest->salidas[i][j] << " -- " << prediccion[j] << " ";
		cout << endl;
		delete[] prediccion;

	}
	// Evaluacion sobre conjunto de test
	testError = test(pDatosTest, NULL, 0, false);
	*errorTest=testError;
	*errorTrain=minTrainError;

}
