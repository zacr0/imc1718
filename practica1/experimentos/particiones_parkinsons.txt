#Fichero de arquitecturas 
#H: marca las neuronas por capa
#L: marca el numero de capas.
#E: factor de aprendizaje
#M: momento
#V: validacion
#D: decremento
#Se hacen todas las combinaciones posibles entre los parametros

#Neuronas
H:50

#Capas
L:2

#Factor aprendizaje
E:0.1

#Momento
M:0.9

#Validacion
V:0.0
V:0.1
V:0.2

#Decremento
D:1.0
D:2.0
