#! /bin/bash
for FILE in "./"*".txt"; do
    gnuplot <<- EOF
        set decimalsign '.'
        set size 1,1
        set term png size 1366,800
        set key top lmargin
        set grid
        set xlabel "Iteración"
        set ylabel "Errores"
        set output "${FILE}_log.png"
        set autoscale y
        set logscale x
        unset logscale y
        plot for [col=2:4] "${FILE}" using 0:col title columnheader with lines
EOF
done